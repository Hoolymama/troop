---
## author, email, creation time, and description will be set automatically at compile time. 
## Uncomment the lines below only if you want to override these fields.
title: Difference Map Tutorial
# author: Someone Else
# email: someone.else@example.com
# created_at: 28 March 2012
description: Create a UV map that represents the distance between 2 objects.
type: Tutorial
---


<% @figure_counter = 1 %>

h3. Introduction

A difference map is a UV map where the value of pixels represent the 3d distance from a UV sample point to an equivalent sample point on another surface. It can be useful for example when pieces of nCloth skin fall from an arm and disintegrate. As the pieces fall further from their start position, the map can be used to drive transparency, particle emission, or some other effect contributing to the disintegration.

This implementation is not a shader, although that may be useful in future. Instead it is a process that directly writes a sequence of images to disk.

h3. Get Started

* Create 2 polygon planes and move one of them up a little in Y.
* Add some animated deformation over 24 frames, for example a wave deformer.
* Choose Litter->Troop->Create Difference Map Sequence to open the UI.


<%= figure_tag('Two Planes and difference Map UI', 'The furthest point between this planes is roughly 0.25 units, so we set Max Distance to 0.3.','images/diffMapSetup.jpg','left') %>

<hr>

* Set parameters as you wish and make sure Max Distance is greater than the largest distance between any eqivalent points on the planes. The resulting map values will be normalized within this distance.
* Make sure both planes are still selected and hit the Apply button.

<%= figure_tag('Difference Map Sequence', 'You can see from this map how the wave deformer offset and bend deformer curvature were animated over 1 second.','images/wavePlanes.jpg','left') %>

<hr>

h3. Velocity Map

If you pick just one object, the difference will be measured between the state of the object at the start and end of each frame, thereby generating a velocity map.

h3. UV Space.

By default the map will represent 0 to 1 in U and V. For any areas in UV where there is no matching geometry, the map will be red. If you only want a small section of UV space to be evaluated, or a section outside 0 - 1, you can set bounds in the UI.

Results may be patchy and unpredictable if there are overlapping UVs.


h3. Done

Have Fun!

