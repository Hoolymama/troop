/***************************************************************************
uvTriData.cpp  -  description
-------------------
    begin                : Mon Apr 3 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com

	a class to store lines in a bounding box for use in a lineKdTree.
	***************************************************************************/

#include "uvTriData.h"
#include "errorMacros.h"

const double TIMESTEP = 1.0f / 24.0f;
const double epsilon = 0.0000001;
const float bigNum = 10e+17;
//const double NEG_EPSILON = -0.0001;

uvTriData::uvTriData(){}

// uvTriData::uvTriData( 		
// 		const float2 &a,
// 		const float2 &b,
// 		const float2 &c,
// 		// unsigned polygonId,
// 		unsigned triangleId
// )
// {
// 	m_a[0] = a[0];m_a[1] = a[1];
// 	m_b[0] = b[0];m_b[1] = b[1];
// 	m_c[0] = c[0];m_c[1] = c[1];

// 	// m_polygonId = polygonId;
// 	m_triangleId = triangleId;
   	
// 	computeBoundingBox();
// }
uvTriData::uvTriData( 		
		const MVector &a,
		const MVector &b,
		const MVector &c,
		unsigned triangleId
)
{
	m_a[0] = float(a.x); m_a[1] = float(a.y);
	m_b[0] = float(b.x); m_b[1] = float(b.y);
	m_c[0] = float(c.x); m_c[1] = float(c.y);
	m_triangleId = triangleId;
   	
	computeBoundingBox();
}

uvTriData::~uvTriData(){}

void uvTriData::computeBoundingBox(){

   	m_min[0] =  bigNum;
   	m_min[1] =  bigNum;
   	m_max[0] = -bigNum;
   	m_max[1] = -bigNum;

   	if (m_a[0] < m_min[0]) { m_min[0] = m_a[0]; } 
   	if (m_a[0] > m_max[0]) { m_max[0] = m_a[0]; }
	if (m_a[1] < m_min[1]) { m_min[1] = m_a[1]; } 
	if (m_a[1] > m_max[1]) { m_max[1] = m_a[1]; }
   	if (m_b[0] < m_min[0]) { m_min[0] = m_b[0]; } 
   	if (m_b[0] > m_max[0]) { m_max[0] = m_b[0]; }
	if (m_b[1] < m_min[1]) { m_min[1] = m_b[1]; } 
	if (m_b[1] > m_max[1]) { m_max[1] = m_b[1]; }
   	if (m_c[0] < m_min[0]) { m_min[0] = m_c[0]; } 
   	if (m_c[0] > m_max[0]) { m_max[0] = m_c[0]; }
	if (m_c[1] < m_min[1]) { m_min[1] = m_c[1]; } 
	if (m_c[1] > m_max[1]) { m_max[1] = m_c[1]; }

   	m_min[0] -= epsilon;
   	m_min[1] -= epsilon;
   	m_max[0] += epsilon;
   	m_max[1] += epsilon;	

   	m_center[0] =  	(m_min[0] + m_max[0]) * 0.5;
   	m_center[1] =  	(m_min[1] + m_max[1]) * 0.5;
}

float  uvTriData::min(axis a) const {return m_min[a];}

float  uvTriData::max(axis a) const {return m_max[a];}

const float2 & uvTriData::center() const {return m_center;}

float uvTriData::center(axis a) const {return m_center[a];}

bool uvTriData::pointInTriangle(const float2 & p, pointOnUvTriInfo & pInfo) const {
	MFloatVector bary;
	bool hit = mayaMath::pointInTriangle2d( m_a, m_b, m_c, p , bary  ) ;
	if (hit) {
		pInfo.setBary(bary);
		pInfo.setTriangleId(m_triangleId);
		// pInfo.setPolygonId(m_polygonId);
		return true;
	}
	return false;
}

