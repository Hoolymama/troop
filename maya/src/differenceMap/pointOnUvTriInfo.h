#ifndef pointOnUvTriInfo_H
#define pointOnUvTriInfo_H

// #include <maya/MIOStream.h>
// #include <math.h>
#include <maya/MFloatVector.h>

#include "errorMacros.h"
#include "mayaMath.h"

// using namespace std;


class pointOnUvTriInfo{
	public:

		pointOnUvTriInfo();

		~pointOnUvTriInfo();

		 // unsigned polygonId() const ;

		 unsigned triangleId() const ;

		const MFloatVector & bary() const ;

		bool valid() const;

		// void setPolygonId( unsigned rhs) ;

		void setTriangleId( unsigned rhs) ;

		void setBary(const MFloatVector & rhs) ;

	private:

		// unsigned int m_polygonId;
		
		unsigned int m_triangleId;

		MFloatVector m_bary;

		bool m_valid;

};

#endif

