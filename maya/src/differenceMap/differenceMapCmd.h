#ifndef	__differenceMapCmd_H__
#define	__differenceMapCmd_H__

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MPxCommand.h>
#include <maya/MItDag.h>
#include <maya/MItSelectionList.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MDynSweptTriangle.h> 

#define kFilenameFlag			"-fn"
#define kFilenameFlagL 			"-filename"

#define kMaxDistFlag			"-md"
#define kMaxDistFlagL 			"-maxDistance"

#define kResFlag				"-res"
#define kResFlagL 				"-resolution"

#define kFormatFlag	         	"-fm"
#define kFormatFlagL          	"-format"

#define kRangeFlag	         	"-rn"
#define kRangeFlagL          	"-range"

/*
#define kCacheFrameFlag			"-c"
#define kCacheFrameFlagL 		"-cacheFrame"

#define kForceCacheFlag			"-fc"
#define kForceCacheFlagL 		"-forceCache"
*/


// #define kFixFlag				"-fx"
// #define kFixFlagL 				"-fix"

/////////////////////////////////////////
//
//	differenceMapCmd
//		: MPxCommand
//


class differenceMapCmd : public MPxCommand

{
public:
	
	differenceMapCmd() {}
	virtual ~differenceMapCmd() {}
	
	MStatus doIt( const MArgList& args );
	
	static void* creator();
	
	static MSyntax      newSyntax();	

	float distanceBetween(
		MDynSweptTriangle &triangleRef,
		MDynSweptTriangle & triangle,
		const MFloatVector & bary,
		float maxDist
		) ;
	
	float distanceBetween(
		MDynSweptTriangle & triangle,
		const MFloatVector & bary,
		float maxDist
		); 


};

#endif	//	!__differenceMapCmd_H__

