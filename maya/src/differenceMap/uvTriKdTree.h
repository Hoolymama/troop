#ifndef _uvTriKdTree
#define _uvTriKdTree

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MString.h>
#include <maya/MVector.h>
#include <maya/MIntArray.h>
#include <maya/MFnDynSweptGeometryData.h>
#include "pointOnUvTriInfo.h" 
#include "uvTriData.h"
#include "errorMacros.h"


class     uvTriData ;

typedef vector<uvTriData*> TRI_LIST;


struct uvTriDataKdNode  {
	bool bucket;							/// is this node a bucket
	axis cutAxis;							/// if not, this axis will divide it 
	double cutVal;							/// at this value
	uvTriDataKdNode *loChild, *hiChild;		/// and these pointers point to the children
	unsigned int loPoint, hiPoint;			/// Indices into the permutations array
	TRI_LIST * overlapList;				/// list of uvTriDatas whose boumding boxes intersect this bucket

	
};


class uvTriKdTree 
{
	public:

		uvTriKdTree();			

		~uvTriKdTree();  

		const uvTriDataKdNode * root();

		uvTriKdTree& operator=(const uvTriKdTree & otherData );

		void 		build(); 							/// build root from m_perm

		uvTriDataKdNode * 	build( int low,  int high ); 	/// build children recursively
		
		int size() ;

		void  	setMaxPointsPerBucket( int b) {
			if (b < 1) {
				m_maxPointsPerBucket = 1;
			} else {
				m_maxPointsPerBucket = b;
			}
		}

		MStatus 	init();

		// MStatus populate( MObject &d) ;
		MStatus populate(const MFnDynSweptGeometryData  &g) ;

		void wirthSelect(  int left,  int right,  int k, axis cutAxis )  ;

		void makeEmpty() ; 
		
		void makeEmpty(uvTriDataKdNode * p) ; 

		void  setOverlapList(uvTriDataKdNode * p,  uvTriData * sph  );
		
		TRI_LIST & uvTriList();

  	// void closestEdge(
  	// 	const uvTriDataKdNode * p,
  	// 	const MPoint &searchPoint,  
  	// 	double & radius, 
  	// 	 uvTriData &  result )  const ;

	  	// void closestEdge(
  		// const uvTriDataKdNode * p,
  		// const MFloatPoint &searchPoint,  
  		// float & radius, 
  		// pointOnEdgeInfo &  result 
  		// )  const ;


	  	void firstHit(
  			const uvTriDataKdNode * p,
  			const float2 &searchPoint,  
  			pointOnUvTriInfo &  result 
  		)  const ;

	private:	
		axis 		findMaxAxis(const  int low, const  int  high) const;
		// void  searchList(const TRI_LIST * overlapList,const MPoint &searchPoint, double & radius, uvTriData & result) const   ;
		// void  searchList(
		// const TRI_LIST * overlapList,
		// const MFloatPoint &searchPoint,
		// float & radius,
		// pointOnEdgeInfo & result  
		// ) const ;

		TRI_LIST * m_perm;				/// pointer to (permutations) - a list of pointers to elements of _uvTriDataVector
		uvTriDataKdNode * m_pRoot;		/// pointer to root of tree
		int m_maxPointsPerBucket;		/// at build time keep recursing until no bucket holds more than this
		
} ;

#endif


