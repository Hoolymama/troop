#ifndef uvTriData_H
#define uvTriData_H

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MDynSweptLine.h>
#include <maya/MBoundingBox.h>
#include <maya/MPointArray.h>
#include <maya/MVectorArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatPointArray.h>

#include <maya/MVector.h>
#include <maya/MDoubleArray.h>
#include <vector>

#include "errorMacros.h"
#include "mayaMath.h"
#include "pointOnUvTriInfo.h" 

using namespace std;

typedef mayaMath::axis axis;
 
class uvTriData{

public:

	uvTriData();

	uvTriData(
		const MVector &a,
		const MVector &b,
		const MVector &c,
		unsigned triangleId
	);

	~uvTriData();

	float  min(axis a) const;

	float  max(axis a) const;

	const float2 & center() const;

	float center(axis a) const;

	bool pointInTriangle(const float2 &p, pointOnUvTriInfo & pInfo) const;

private:
		
 	void computeBoundingBox();
   	
   	float2 m_a; 
   	float2 m_b; 
   	float2 m_c; 

   	// unsigned int m_polygonId;

   	unsigned int m_triangleId;

   	float2 m_min;
   	float2 m_max;
   	float2 m_center;

};

#endif

