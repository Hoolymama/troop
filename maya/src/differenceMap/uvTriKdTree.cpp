
/***************************************************************************
    uvTriKdTree.cpp  	:-  description
    begin               : Mon Apr 3 2006
    copyright           : (C) 2006 by Julian Mann
    email               : julian.mann@gmail.com
 ***************************************************************************/

#define P_TOLERANCE 0.000001
// #include <maya/MItMeshEdge.h>
#include <maya/MDynSweptTriangle.h>

#include "uvTriKdTree.h"

uvTriKdTree::uvTriKdTree()
	: m_perm(0),
	  m_pRoot(0),
	  m_maxPointsPerBucket(4)
{
	m_pRoot = 0;
	m_perm = new TRI_LIST;

}

uvTriKdTree::~uvTriKdTree()
{
	if (m_perm) {
		TRI_LIST::iterator p =  m_perm->begin();
		while (p != m_perm->end()) {
			delete *p;
			*p = 0;
			p++;
		}
		delete m_perm;
		m_perm = 0;
	}
	makeEmpty(m_pRoot);
}

void uvTriKdTree::makeEmpty(uvTriDataKdNode *p) {
	if (p) {
		if (!(p->bucket)) {
			makeEmpty(p->loChild);	//
			makeEmpty(p->hiChild);
		}
		else {
			delete p->overlapList;
			p->overlapList  = 0;
		}
		delete p;
		p = 0;
	}
}

const uvTriDataKdNode *uvTriKdTree::root() {
	return m_pRoot;
};

MStatus uvTriKdTree::populate(const MFnDynSweptGeometryData  &g)  {
	MStatus st;
	unsigned int len = g.triangleCount(&st); mser;
	if (!len) { return MS::kFailure; }
	// cerr << "num triangles:" << len << endl;
	for (unsigned i = 0; i < len; i++) {
		MDynSweptTriangle tg =  g.sweptTriangle(i);
		MVector a = tg.uvPoint(0);
		MVector b = tg.uvPoint(1);
		MVector c = tg.uvPoint(2);
		// cerr << "---------------------------" << endl;
		// cerr << "A: " << a.x << " " << a.y << endl;
		// cerr << "B: " << b.x << " " << b.y << endl;
		// cerr << "C: " << c.x << " " << c.y << endl;

		uvTriData *t = new uvTriData(a, b, c, i);
		m_perm->push_back(t) ;
	}
	return MS::kSuccess;
}

int uvTriKdTree::size() {
	return int(m_perm->size());
}

void uvTriKdTree::build() {
	int low = 0;
	int high = (size() - 1);
	m_pRoot = build(low, high);

	TRI_LIST::iterator currentBox = m_perm->begin();
	while (currentBox != m_perm->end()) {
		setOverlapList(m_pRoot , *currentBox);
		currentBox++;
	}
}


uvTriDataKdNode   *uvTriKdTree::build( int low,  int high	) {

	uvTriDataKdNode *p = new uvTriDataKdNode;

	if (((high - low) + 1) <= m_maxPointsPerBucket) {
		// only bucket nodes will hold an overlapList
		p->bucket = true;
		p->loPoint = low;
		p->hiPoint = high;
		p->loChild = 0;
		p->hiChild = 0;
		p->overlapList = new TRI_LIST;
	}
	else {

		p->bucket = false;
		p->cutAxis = findMaxAxis(low, high);
		int mid = ((low + high) / 2);
		wirthSelect(low, high, mid, p->cutAxis);
		p->cutVal = ((*m_perm)[mid])->center(p->cutAxis);
		p->loChild = build(low, mid);
		p->hiChild = build(mid + 1, high);
	}
	return p;
}


void uvTriKdTree::wirthSelect(  int left,  int right,  int k, axis cutAxis )
{
	int n = (right - left) + 1;
	if (n <= 1) { return; }
	int i, j, l, m;
	uvTriData *x;
	uvTriData *tmp;

	l = left;
	m = right;
	while (l < m) {
		x = (*m_perm)[k];
		i = l;
		j = m;
		do {
			while (  ((*m_perm)[i])->center(cutAxis) <  x->center(cutAxis)  ) { i++; }
			while (  ((*m_perm)[j])->center(cutAxis) >  x->center(cutAxis)  ) { j--; }

			if (i <= j) {
				// swap
				tmp = (*m_perm)[i];
				(*m_perm)[i] = (*m_perm)[j] ;
				(*m_perm)[j] = tmp ;
				i++; j--;
			}
		}
		while (i <= j);
		if (j < k) { l = i; }
		if (k < i) { m = j; }
	}
}

axis uvTriKdTree::findMaxAxis(const  int low, const  int high) const {

	// The idea here is just to find the axis containing the longest
	// side of the bounding rectangle of the points

	// From a vector of N points we just take sqrtN samples
	// in order to keep the time down to O(N)
	// should be ok though
	float minx , miny,  maxx , maxy,  tmpVal;
	float sx, sy ;

	int  num = (high - low ) + 1;
	int interval = int(sqrt(double(num)));
	// unsigned int intervalSq = interval*interval;
	int i;
	float px, py ;

	minx = ((*m_perm)[low])->center(mayaMath::xAxis);
	maxx = minx;
	miny = ((*m_perm)[low])->center(mayaMath::yAxis);
	maxy = miny;

	for (i = (low + interval); i <= high; i += interval ) {
		px = ((*m_perm)[i])->center(mayaMath::xAxis);
		py = ((*m_perm)[i])->center(mayaMath::yAxis);
		if (px < minx) {
			minx = px;
		}
		else if (px > maxx) {
			maxx = px;
		}
		if (py < miny) {
			miny = py;
		}
		else if (py > maxy) {
			maxy = py;
		}
	}
	sx = maxx - minx;
	sy = maxy - miny;

	if (sx > sy) {
		return mayaMath::xAxis;
	}
	else {
		return mayaMath::yAxis;
	}
}

void  uvTriKdTree::setOverlapList(uvTriDataKdNode *p,  uvTriData *tb  )  {
	// recursive setOverlapList method
	// put box in every bucket it overlaps
	if (p->bucket) {
		p->overlapList->push_back(tb);
	}
	else {
		if (tb->min(p->cutAxis) < p->cutVal) {
			setOverlapList(p->loChild, tb);
		}
		if (tb->max(p->cutAxis) > p->cutVal) {
			setOverlapList(p->hiChild, tb);
		}
	}
}

// recurse to find the a triangle we are in
void uvTriKdTree::firstHit(
  const uvTriDataKdNode *p,
  const float2 &searchPoint,
  pointOnUvTriInfo &result
)  const  {
	if (p->bucket) {
		// search the overlap list - return on first find
		TRI_LIST::const_iterator curr;
		curr = p->overlapList->begin();
		while (curr != p->overlapList->end()) {
			if ( (*curr)->pointInTriangle(searchPoint, result)) {
				return ;
			}
			curr++;
		}
	}
	else {
		double diff = searchPoint[(p->cutAxis)] -
		              p->cutVal; // distance to the cut wall for this bucket
		if (diff < 0) { // we are in the lo child so search it
			firstHit(p->loChild, searchPoint, result);
		}
		else {   // we are in the hi child so search it
			firstHit(p->hiChild, searchPoint, result);
		}
	}
}

TRI_LIST &uvTriKdTree::uvTriList() {
	return *m_perm;
}

