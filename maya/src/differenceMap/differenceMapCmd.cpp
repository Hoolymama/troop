#include <maya/MPlug.h>
#include <maya/MTypeId.h>
#include <maya/MFnDynSweptGeometryData.h>
#include <maya/MImage.h>

#include <maya/MGlobal.h>



#include <errorMacros.h>
#include "uvTriKdTree.h"

#include "differenceMapCmd.h"




//	static
void *differenceMapCmd::creator()
{
	return new differenceMapCmd();
}

MSyntax differenceMapCmd::newSyntax()
{
	MSyntax syn;



	//syn.addFlag(kCacheFrameFlag, kCacheFrameFlagL);
	syn.addFlag(kFilenameFlag, kFilenameFlagL, MSyntax::kString );
	syn.addFlag(kMaxDistFlag, kMaxDistFlagL, MSyntax::kDouble );
	syn.addFlag(kResFlag, kResFlagL, MSyntax::kDouble, MSyntax::kDouble );
	syn.addFlag(kFormatFlag, kFormatFlagL,  MSyntax::kString);
	syn.addFlag(kRangeFlag, kRangeFlagL, MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble,
	            MSyntax::kDouble);


	//syn.addFlag(kForceCacheFlag, kForceCacheFlagL);
	// syn.addFlag(kFixFlag, kFixFlagL);

	syn.setObjectType(MSyntax::kSelectionList, 1, 2);
	syn.useSelectionAsDefault(true);
	// syn.enableQuery(false);
	// syn.enableEdit(true);
	return syn;

}
float differenceMapCmd::distanceBetween(
  MDynSweptTriangle &triangleRef,
  MDynSweptTriangle &triangle,
  const MFloatVector &bary,
  float maxDist
) {
	MVector r0 = triangleRef.vertex(0);
	MVector r1 = triangleRef.vertex(1);
	MVector r2 = triangleRef.vertex(2);
	MVector t0 = triangle.vertex(0);
	MVector t1 = triangle.vertex(1);
	MVector t2 = triangle.vertex(2);

	MFloatVector pr = MFloatVector((r0.x * bary.x + r1.x * bary.y + r2.x * bary.z) ,
	                               (r0.y * bary.x + r1.y * bary.y + r2.y * bary.z) ,
	                               (r0.z * bary.x + r1.z * bary.y + r2.z * bary.z));
	MFloatVector pt = MFloatVector((t0.x * bary.x + t1.x * bary.y + t2.x * bary.z) ,
	                               (t0.y * bary.x + t1.y * bary.y + t2.y * bary.z) ,
	                               (t0.z * bary.x + t1.z * bary.y + t2.z * bary.z));
	float d = (pt - pr).length() / maxDist;
	if (d > 1.0f) { d = 1.0f; }
	return d;
}

float differenceMapCmd::distanceBetween(
  MDynSweptTriangle &triangle,
  const MFloatVector &bary,
  float maxDist
) {
	MVector r0 = triangle.vertex(0, 0.0);
	MVector r1 = triangle.vertex(1, 0.0);
	MVector r2 = triangle.vertex(2, 0.0);
	MVector t0 = triangle.vertex(0, 1.0);
	MVector t1 = triangle.vertex(1, 1.0);
	MVector t2 = triangle.vertex(2, 1.0);

	MFloatVector pr = MFloatVector((r0.x * bary.x + r1.x * bary.y + r2.x * bary.z) ,
	                               (r0.y * bary.x + r1.y * bary.y + r2.y * bary.z) ,
	                               (r0.z * bary.x + r1.z * bary.y + r2.z * bary.z));
	MFloatVector pt = MFloatVector((t0.x * bary.x + t1.x * bary.y + t2.x * bary.z) ,
	                               (t0.y * bary.x + t1.y * bary.y + t2.y * bary.z) ,
	                               (t0.z * bary.x + t1.z * bary.y + t2.z * bary.z));
	float d = (pt - pr).length() / maxDist;
	if (d > 1.0f) { d = 1.0f; }
	return d;
}



MStatus differenceMapCmd::doIt( const MArgList &args )
{
	MStatus st;
	//	unsigned int counter = 0;
	MString method("differenceMapCmd::doIt");

	MSelectionList list;
	MArgDatabase  argData(syntax(), args);

	if (! argData.isFlagSet(kFilenameFlag)) {
		MGlobal::displayError("Need a filename");
		return MS::kUnknownParameter;
	}
	MString filename;
	st = argData.getFlagArgument (kFilenameFlag, 0, filename); msert;


	double ddist = 1.0;
	if (argData.isFlagSet(kMaxDistFlag)) {
		st = argData.getFlagArgument (kMaxDistFlag, 0, ddist); msert;
	}
	float dist = float(ddist);

	unsigned resolutionX = 512;
	unsigned resolutionY = 512;
	if (argData.isFlagSet(kResFlag)) {
		st = argData.getFlagArgument (kResFlag, 0, resolutionX); msert;
		st = argData.getFlagArgument (kResFlag, 1, resolutionY); msert;
	}

	double rangeMinU = 0.0;
	double rangeMinV = 0.0;
	double rangeMaxU = 1.0;
	double rangeMaxV = 1.0;
	if (argData.isFlagSet(kRangeFlag)) {
		st = argData.getFlagArgument (kRangeFlag, 0, rangeMinU); msert;
		st = argData.getFlagArgument (kRangeFlag, 1, rangeMinV); msert;
		st = argData.getFlagArgument (kRangeFlag, 2, rangeMaxU); msert;
		st = argData.getFlagArgument (kRangeFlag, 3, rangeMaxV); msert;
	}



	MString format("iff");
	if (argData.isFlagSet(kFormatFlag)) {
		st = argData.getFlagArgument (kFormatFlag, 0, format); msert;
		if (!(
		      (format == "bmp") ||
		      (format == "cin") ||
		      (format == "gif") ||
		      (format == "jpg") ||
		      (format == "rla") ||
		      (format == "sgi") ||
		      (format == "tga") ||
		      (format == "tif") ||
		      (format == "iff")
		    ))
		{
			format = "iff";
		}
	}


	argData.getObjects(list);

	unsigned numObjects = list.length();

	MItSelectionList geoIter( list, MFn::kDependencyNode);
	MObject node1;
	MObject node2;

	if (geoIter.isDone()) { return MS::kFailure; }


	MFnDynSweptGeometryData fnSweptDataRef;
	if (numObjects == 2) {
		geoIter.getDependNode( node1 );
		MFnDependencyNode node1Fn( node1 );
		MPlug plug1 = node1Fn.findPlug("sweptGeometry", true, &st); msert;
		MObject dSweptData1;
		st = plug1.getValue(dSweptData1);
		st = fnSweptDataRef.setObject(dSweptData1); msert;
		geoIter.next();
	}


	geoIter.getDependNode( node2 );
	MFnDependencyNode node2Fn( node2 );
	MPlug plug2 = node2Fn.findPlug("sweptGeometry", true, &st); msert;
	MObject dSweptData2;
	st = plug2.getValue(dSweptData2);
	MFnDynSweptGeometryData fnSweptData(dSweptData2, &st); msert;
	unsigned len = fnSweptData.triangleCount();

	cerr << "fnSweptData.triangleCount()" << fnSweptData.triangleCount() << endl;
	cerr << "fnSweptDataRef.triangleCount()" << fnSweptDataRef.triangleCount() << endl;

	if ((numObjects == 2) && (len != fnSweptDataRef.triangleCount())) {
		MGlobal::displayError("If 2 objects given, they must have matching topology");
		return MS::kUnknownParameter;
	}

	//cerr << "fnSweptDataRef.triangleCount() " << len << endl;

	uvTriKdTree *pTree = new uvTriKdTree();

	st = pTree->populate(fnSweptData);
	pTree->build();
	if (! pTree->size()) {
		delete pTree;
		pTree = 0;
		return MS::kFailure;
	}
	//cerr << "pTree->size() " << pTree->size() << endl;

	MImage rgbaImage;
	st = rgbaImage.create(resolutionX, resolutionY); msert;

	unsigned int numPixels = resolutionX * resolutionY;
	unsigned char *colorPixels = new unsigned char[numPixels * 4];
	if (!colorPixels) {
		delete [] colorPixels;
		delete pTree;
		pTree = 0;
		return MS::kFailure;
	}

	//JPMDBG;

	int lastTriangleId = -1;
	MDynSweptTriangle triangle;
	MDynSweptTriangle triangleRef;

	unsigned char *cPtr = colorPixels;
	double rangeU = rangeMaxU - rangeMinU;
	double rangeV = rangeMaxV - rangeMinV;


	for (unsigned v = 0; v < resolutionY; v++) {
		for (unsigned u = 0; u < resolutionX; u++) {
			unsigned char colR = (unsigned char)(255.0f);
			unsigned char colG = (unsigned char)(0.0f);
			unsigned char colB = (unsigned char)(0.0f);
			unsigned char colA = (unsigned char)(255.0f);
			pointOnUvTriInfo pInfo;
			float2 uvCoord;

			uvCoord[0] = float(rangeMinU + (((u + 0.5) / double(resolutionX)) * rangeU)) ;
			uvCoord[1] =  float(rangeMinV + (((v + 0.5) / double(resolutionY)) * rangeV)) ;

			pTree->firstHit(pTree->root(), uvCoord, pInfo);


			if (pInfo.valid()) {
				MFloatVector bary = pInfo.bary();
				int triangleId = pInfo.triangleId();
				triangle = fnSweptData.sweptTriangle(triangleId, &st); mser;
				if (numObjects == 2) { triangleRef = fnSweptDataRef.sweptTriangle(triangleId, &st); } mser;
				float d ;
				if (numObjects == 2) {
					d = distanceBetween(triangleRef, triangle, pInfo.bary(), dist);
					//cerr << d << " ";
				}
				else {
					d = distanceBetween(triangle, pInfo.bary(), dist);
				}

				colR = (unsigned char)(255.0f * d);
				colG = colR;
				colB = colR;

			}

			*cPtr = colR; cPtr++; *cPtr = colG; cPtr++; *cPtr = colB; cPtr++; *cPtr = colA; cPtr++;
		}
	}

	rgbaImage.setPixels( colorPixels, resolutionX, resolutionY );

	rgbaImage.writeToFile(filename , format);

	delete [] colorPixels;

	delete pTree;
	pTree = 0;
	rgbaImage.release();

	return MS::kSuccess;
}

