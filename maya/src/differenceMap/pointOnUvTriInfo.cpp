#include "pointOnUvTriInfo.h"
#include "errorMacros.h"

pointOnUvTriInfo::pointOnUvTriInfo():m_valid(false){}
pointOnUvTriInfo::~pointOnUvTriInfo() {}

// void 	pointOnUvTriInfo::setPolygonId( unsigned rhs) {m_polygonId = rhs;} 
void 	pointOnUvTriInfo::setTriangleId( unsigned rhs) {m_triangleId = rhs;} 
void 	pointOnUvTriInfo::setBary(const MFloatVector & rhs) {m_bary = rhs; m_valid = true;} 

// unsigned  pointOnUvTriInfo::polygonId() const {return m_polygonId;}
unsigned  pointOnUvTriInfo::triangleId() const {return m_triangleId;} 
const MFloatVector & pointOnUvTriInfo::bary() const {return m_bary;};
bool 	pointOnUvTriInfo::valid() const {return m_valid; }


