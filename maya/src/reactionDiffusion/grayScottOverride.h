#ifndef grayScottOverride_H
#define grayScottOverride_H

#include <maya/MString.h>
#include <maya/MObject.h>
#include <maya/MPxShadingNodeOverride.h>
#include "grayScott.h"
class grayScottOverride : public MHWRender::MPxShadingNodeOverride
{
public:
	static MHWRender::MPxShadingNodeOverride *creator(const MObject &obj);

	virtual ~grayScottOverride();

	virtual MHWRender::DrawAPI supportedDrawAPIs() const;

	virtual MString fragmentName() const;
	virtual void getCustomMappings(
	  MHWRender::MAttributeParameterMappingList &mappings);

	virtual void updateDG();
	virtual void updateShader(
	  MHWRender::MShaderInstance &shader,
	  const MHWRender::MAttributeParameterMappingList &mappings);


private:
	grayScottOverride(const MObject &obj);

	MString fFragmentName;
	MObject fObject;
	MString fFileName;
	grayScott *m_solver;

	const MHWRender::MSamplerState *fSamplerState;

	mutable MString fResolvedMapName;
	mutable MString fResolvedSamplerName;
};



#endif