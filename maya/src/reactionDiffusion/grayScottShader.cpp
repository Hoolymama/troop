#include <maya/MIOStream.h>
#include <math.h>
#include <maya/MPxNode.h>
#include <maya/MStatus.h>

#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnEnumAttribute.h>


#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnPluginData.h>
#include <maya/MFloatVector.h>
#include <maya/MDoubleArray.h>
#include <maya/MVector.h>
#include <maya/MPointArray.h>
#include <maya/MFloatPoint.h>
#include <maya/MRampAttribute.h>
#include <maya/MVectorArray.h>
#include <maya/MTime.h>

#include "errorMacros.h"
#include "jMayaIds.h"

#include "grayScottData.h"

#include "grayScottShader.h"
#include "imageUtils.h"



MObject grayScottShader::aGrayScottData;
MObject grayScottShader::aUVCoord;
MObject grayScottShader::aSampleInterpolation;
MObject grayScottShader::aDefaultColor 	;
MObject grayScottShader::aOutColor;
MObject grayScottShader::aOutAlpha;


MTypeId grayScottShader::id( k_grayScottShader );


grayScottShader::grayScottShader()
{
}

grayScottShader::~grayScottShader()
{
}


void *grayScottShader::creator()
{
	return new grayScottShader();
}
void grayScottShader::postConstructor( )
{
	setMPSafe(false);
}


MStatus grayScottShader::initialize()
{
	MStatus st;




	MString method("grayScottShader::initialize");
	MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnUnitAttribute uAttr;
	MFnEnumAttribute eAttr;


	aSampleInterpolation = eAttr.create("sampleInterpolation", "sin");
	eAttr.addField("Nearest", imageUtils::kNearest);
	eAttr.addField("Bilinear", imageUtils::kBilinear);

	eAttr.setDefault( imageUtils::kBilinear );
	eAttr.setKeyable(true);
	eAttr.setWritable(true);
	addAttribute(aSampleInterpolation);



	aGrayScottData = tAttr.create("grayScottData", "gsd", grayScottData::id ) ;
	tAttr.setStorable(false);
	tAttr.setCached(false);
	st = addAttribute( aGrayScottData ); mser;


	MObject c1 = nAttr.create("uCoord", "u", MFnNumericData::kFloat);
	MObject c2 = nAttr.create("vCoord", "v", MFnNumericData::kFloat);
	aUVCoord = nAttr.create("uvCoord", "uv", c1, c2);
	nAttr.setKeyable(true);
	nAttr.setStorable(false);
	nAttr.setReadable(true);
	nAttr.setWritable(true);
	nAttr.setHidden(true);
	addAttribute(aUVCoord);


	aDefaultColor = nAttr.createColor( "defaultColor", "dc" );
	nAttr.setDefault(1.0f, 0.0f, 0.0f);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setUsedAsColor(true);
	nAttr.setReadable(true);
	nAttr.setWritable(true);
	st = addAttribute( aDefaultColor ); mser;

	aOutColor = nAttr.createColor( "outColor", "oc" );
	nAttr.setWritable( false );
	nAttr.setKeyable( false );
	nAttr.setReadable( true);
	nAttr.setStorable( false);
	st = addAttribute( aOutColor ); mser;


	aOutAlpha = nAttr.create( "outAlpha", "oa", MFnNumericData::kFloat);
	nAttr.setKeyable(false);
	nAttr.setStorable(false);
	nAttr.setReadable(true);
	nAttr.setWritable(false);
	addAttribute(aOutAlpha);

	attributeAffects (aUVCoord, aOutColor);
	attributeAffects (aGrayScottData, aOutColor);
	attributeAffects (aDefaultColor, aOutColor);
	attributeAffects (aSampleInterpolation, aOutColor);

	attributeAffects (aUVCoord, aOutAlpha);
	attributeAffects (aGrayScottData, aOutAlpha);
	attributeAffects (aDefaultColor, aOutAlpha);
	attributeAffects (aSampleInterpolation, aOutAlpha);

	return MS::kSuccess;
}


MStatus grayScottShader::compute( const MPlug  &plug,  MDataBlock &data )
{
	MStatus st;

	MString method("grayScottShader::compute");

	if ((plug != aOutColor) && (plug.parent() != aOutColor)
	    && (plug != aOutAlpha)) 	{ return MS::kUnknownParameter; }

	MFloatVector &defaultColor = data.inputValue(aDefaultColor).asFloatVector();

	imageUtils::Interpolation interp = imageUtils::Interpolation(data.inputValue(
	                                     aSampleInterpolation).asShort());

	MFloatVector resultColor = defaultColor;
	float resultAlpha = 0.0f;

	float2 &uv = data.inputValue(aUVCoord).asFloat2();

	grayScottData *solverData = 0;
	// JPMDBG;
	MDataHandle hSolverData = data.inputValue(aGrayScottData, &st); msert;
	// JPMDBG;

	MObject dSolverData = hSolverData.data();
	// JPMDBG;
	MFnPluginData fnSolverData( dSolverData, &st );
	// JPMDBG;
	if (!(st.error())) {
		// JPMDBG;
		solverData = (grayScottData *)fnSolverData.data();
		//cerr << "solverData " << solverData << endl;
		if (solverData->valid()) {
			grayScott *solver = solverData->solver();

			float result[2];

			st =  solver->sample( uv[0],  uv[1], result, interp);

			resultColor =  MFloatVector(result[0], result[1], 0.0);

		}
	}

	///////////////////////////////////////////////////////////////
	MDataHandle outAlphaHandle = data.outputValue( aOutAlpha );
	float &outAlpha = outAlphaHandle.asFloat();
	outAlpha = resultAlpha;
	outAlphaHandle.setClean();

	MDataHandle outColorHandle = data.outputValue( aOutColor );
	MFloatVector &outColor = outColorHandle.asFloatVector();
	outColor = resultColor;
	outColorHandle.setClean();

	return MS::kSuccess;
}