#ifndef imageUtils_H
#define imageUtils_H



#include <math.h>
#include <maya/MFloatVector.h>
#include <maya/MString.h>
#include <maya/MImage.h>
#include "errorMacros.h"


class imageUtils {


public:

	enum Interpolation { kNearest, kBilinear, kBiCubic };



	static MStatus resize(unsigned width, unsigned height, MImage &image);

	static void  bilinearSample(float u, float v, const float *pixels ,
	                            unsigned w, unsigned h,  unsigned channels,  float *result );



	static void  nearestSample(float u, float v, const float *pixels ,
	                           unsigned w, unsigned h,   unsigned channels, float *result );

	static void sample(float u, float v, const float *pixels , unsigned w,
	                   unsigned h, unsigned channels, float *result,
	                   Interpolation interp = imageUtils::kNearest);

	static float lerp(float s, float e, float t);

	static float  blerp(float c00, float c10, float c01, float c11,  float tx,
	                    float ty);

	static   size_t cellIndex(int x, int y, unsigned w, unsigned h)  ;

	static   const float *cellVals(int x, int y,  unsigned w, unsigned h,
	                               const float *pixels)   ;



};



#endif


