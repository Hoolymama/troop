#include <maya/MFnNumericAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnPluginData.h>
#include <maya/MAnimControl.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MRenderUtil.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnEnumAttribute.h>

#include <maya/MFloatMatrix.h>

#include <maya/MFloatArray.h>

#include <maya/MPlugArray.h>

#include "errorMacros.h"
#include "grayScottData.h"
#include "grayScottNode.h"
#include "jMayaIds.h"


MTypeId     grayScottNode::id( k_grayScottNode );

MObject 	grayScottNode::aFeedRate;
MObject 	grayScottNode::aKillRate;
MObject 	grayScottNode::aDiffA;
MObject 	grayScottNode::aDiffB;
MObject 	grayScottNode::aSteps;

MObject 	grayScottNode::aKernelTop;
MObject 	grayScottNode::aKernelMiddle;
MObject 	grayScottNode::aKernelBottom;

MObject 	grayScottNode::aFeedKillMap;
MObject 	grayScottNode::aDiffMap;
MObject 	grayScottNode::aKernelSlopeMap;
MObject 	grayScottNode::aKernelSlopeAngle;

MObject 	grayScottNode::aMapUpdate;
MObject 	grayScottNode::aMapSampleResolution;

MObject 	grayScottNode::aSeedImage;
MObject 	grayScottNode::aCurrentTime;
MObject 	grayScottNode::aStartTime;
MObject 	grayScottNode::aOutput;


grayScottNode::grayScottNode()
	: m_lastTimeIEvaluated(MAnimControl::currentTime())
{
	m_gsData = new grayScottData;
}

grayScottNode::~grayScottNode() {

	if (m_gsData) { delete m_gsData; }

	m_gsData = 0;
}

void *grayScottNode::creator()
{
	return new grayScottNode();
}

MStatus grayScottNode::initialize()
{
	MStatus st;

	MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnUnitAttribute uAttr;
	MFnEnumAttribute eAttr;

	aSeedImage = tAttr.create("seedImage", "si", MFnData::kString);
	tAttr.setStorable(true);
	tAttr.setUsedAsFilename(true);
	addAttribute( aSeedImage );

	aSteps = nAttr.create( "iterations", "it", MFnNumericData::kInt);
	nAttr.setStorable(true);
	nAttr.setReadable(true);
	nAttr.setDefault(1);
	nAttr.setKeyable(true);
	addAttribute(aSteps);

	aMapUpdate = eAttr.create("updateCoefficients", "uc");
	eAttr.addField("seedFrame", grayScottNode::kSeedFrame);
	eAttr.addField("allFrames", grayScottNode::kAllFrames);
	eAttr.setDefault( grayScottNode::kSeedFrame );
	eAttr.setKeyable(true);
	eAttr.setWritable(true);
	addAttribute(aMapUpdate);

	aMapSampleResolution = nAttr.create( "mapSampleResolution", "msr", MFnNumericData::kInt);
	nAttr.setStorable( true );
	nAttr.setKeyable( true );
	nAttr.setReadable( true );
	nAttr.setWritable( true );
	nAttr.setKeyable( true );
	nAttr.setMin(2);
	nAttr.setMax(2048);
	addAttribute( aMapSampleResolution );

	aKernelTop = nAttr.create("kernelTop", "kt", MFnNumericData::k3Float);
	nAttr.setHidden( false );
	nAttr.setKeyable( true );
	nAttr.setDefault( 0.0, 1.0, 0.0 );
	st = addAttribute( aKernelTop ); mser;

	aKernelMiddle = nAttr.create("kernelMiddle", "km", MFnNumericData::k3Float);
	nAttr.setHidden( false );
	nAttr.setKeyable( true );
	nAttr.setDefault( 1.0, 0.0, 1.0 );
	st = addAttribute( aKernelMiddle ); mser;

	aKernelBottom = nAttr.create("kernelBottom", "kb", MFnNumericData::k3Float);
	nAttr.setHidden( false );
	nAttr.setKeyable( true );
	nAttr.setDefault( 0.0, 1.0, 0.0 );
	st = addAttribute( aKernelBottom ); mser;

	aKernelSlopeMap = nAttr.create( "kernelSlopeMap", "ksm", MFnNumericData::kFloat);
	nAttr.setStorable( true );
	nAttr.setKeyable( true );
	nAttr.setReadable( true );
	nAttr.setWritable( true );
	nAttr.setKeyable( true );
	addAttribute( aKernelSlopeMap );

	aKernelSlopeAngle = uAttr.create("kernelSlopeAngle", "ksa",  MFnUnitAttribute::kAngle, 0);
	uAttr.setHidden( false );
	eAttr.setKeyable(true);
	eAttr.setWritable(true);
	addAttribute(aKernelSlopeAngle);

	aFeedKillMap = nAttr.createColor( "feedKillMap", "fkm");
	nAttr.setStorable(true);
	nAttr.setReadable( true );
	nAttr.setWritable( true );
	nAttr.setKeyable(true);
	nAttr.setConnectable(true);
	addAttribute(aFeedKillMap);

	aDiffMap = nAttr.createColor( "diffMap", "dfm");
	nAttr.setStorable(true);
	nAttr.setReadable( true );
	nAttr.setWritable( true );
	nAttr.setKeyable(true);
	nAttr.setConnectable(true);
	addAttribute(aDiffMap);



	aFeedRate = nAttr.create( "feedRate", "feed", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	nAttr.setReadable(true);
	nAttr.setDefault(0.0545);
	nAttr.setKeyable(true);
	addAttribute(aFeedRate);

	aKillRate = nAttr.create( "killRate", "kill", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	nAttr.setReadable(true);
	nAttr.setDefault(0.062);
	nAttr.setKeyable(true);
	addAttribute(aKillRate);

	aDiffA = nAttr.create( "diffA", "da", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	nAttr.setReadable(true);
	nAttr.setDefault(0.2);
	nAttr.setKeyable(true);
	addAttribute(aDiffA);

	aDiffB = nAttr.create( "diffB", "db", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	nAttr.setReadable(true);
	nAttr.setDefault(0.1);
	nAttr.setKeyable(true);
	addAttribute(aDiffB);


	aCurrentTime = uAttr.create( "currentTime", "ct", MFnUnitAttribute::kTime );
	uAttr.setStorable(true);
	st =  addAttribute(aCurrentTime);  mser;

	aStartTime = uAttr.create( "startTime", "st", MFnUnitAttribute::kTime );
	uAttr.setStorable(true);
	st =  addAttribute(aStartTime);  mser;


	aOutput = tAttr.create("output", "out", grayScottData::id);
	tAttr.setReadable(true);
	tAttr.setStorable(false);
	tAttr.setCached(false);
	addAttribute(aOutput);

	st = attributeAffects(aCurrentTime, aOutput ); mser;
	st = attributeAffects(aStartTime, aOutput ); mser;
	st = attributeAffects(aSeedImage, aOutput ); mser;

	return MS::kSuccess;
}



MStatus grayScottNode::getTextureName(const MObject &attribute,
                                      MString &name) const {
	MStatus st;
	MPlugArray plugArray;
	MPlug plug(thisMObject(), attribute);
	bool hasConnection = plug.connectedTo(plugArray, 1, 0, &st);
	if (st.error()) { return st; }
	if (! hasConnection) { return MS::kUnknownParameter; }
	name = plugArray[0].name(&st);
	return MS::kSuccess;
}



MStatus grayScottNode::sampleTexture(const MObject &attribute,
                                     int resolution, MFloatArray &result) const {

	MStatus st;
	MString plugName;
	st = getTextureName(attribute, plugName); msert;
	MFloatMatrix cameraMat;
	cameraMat.setToIdentity();
	MFloatVectorArray colors;
	MFloatVectorArray transparencies;

	if (resolution < 1) {
		return MS::kFailure;
	}

	int n = resolution * resolution;
	MFloatArray uVals(n);
	MFloatArray vVals(n);
	result.setLength(n);
	for (int i = 0, y = 0; y < resolution; y++) {
		float vVal =  (y + 0.5) / resolution;
		for (int x = 0; x < resolution; x++) {
			float uVal = (x + 0.5) / resolution;
			uVals.set( uVal, i );
			vVals.set( vVal, i );
			i++;
		}
	}
	st =  MRenderUtil::sampleShadingNetwork (plugName, n, false, false, cameraMat,
	      0, &uVals, &vVals, 0, 0, 0, 0, 0, colors, transparencies); msert;

	for (int i = 0; i < n; ++i)
	{
		/* set result to the color red channel */
		result.set(colors[i][0], i);
	}

	return MS::kSuccess;
}


MStatus grayScottNode::sampleTexture(const MObject &attribute,
                                     int resolution, MFloatVectorArray &result) const {

	MStatus st;
	MString plugName;
	st = getTextureName(attribute, plugName); msert;
	MFloatMatrix cameraMat;
	cameraMat.setToIdentity();
	MFloatVectorArray transparencies;

	if (resolution < 1) {
		return MS::kFailure;
	}

	int n = resolution * resolution;
	MFloatArray uVals(n);
	MFloatArray vVals(n);
	for (int i = 0, y = 0; y < resolution; y++) {
		float vVal =  (y + 0.5) / resolution;
		for (int x = 0; x < resolution; x++) {
			float uVal = (x + 0.5) / resolution;
			uVals.set( uVal, i );
			vVals.set( vVal, i );
			i++;
		}
	}
	st =  MRenderUtil::sampleShadingNetwork (plugName, n, false, false, cameraMat,
	      0, &uVals, &vVals, 0, 0, 0, 0, 0, result, transparencies); msert;

	return MS::kSuccess;
}

MStatus grayScottNode::setKernelImage(MDataBlock &data, int sampleResolution ) {

	MStatus st = MS::kSuccess;

	float angle = float(data.inputValue( aKernelSlopeAngle).asAngle().value()) ;

	if (sampleResolution > 2048) { sampleResolution = 2048; }
	MFloatVector knTop = data.inputValue(aKernelTop).asFloatVector();
	MFloatVector knMid = data.inputValue(aKernelMiddle).asFloatVector();
	MFloatVector knBot = data.inputValue(aKernelBottom).asFloatVector();

	MFloatArray kernelSlopeValues;
	st = sampleTexture(aKernelSlopeMap, sampleResolution, kernelSlopeValues);
	if (st.error()) {
		kernelSlopeValues.clear();
	}

	return m_gsData->solver()->setKernelImage(kernelSlopeValues, knTop,
	       knMid,  knBot, angle);
}

MStatus grayScottNode::setFeedKillImage(MDataBlock &data,
                                        int sampleResolution ) {
	MStatus st = MS::kSuccess;
	if (sampleResolution > 2048) { sampleResolution = 2048; }
	float feed = data.inputValue(aFeedRate).asFloat();
	float kill = data.inputValue(aKillRate).asFloat();
	MFloatVectorArray values;
	st = sampleTexture(aFeedKillMap, sampleResolution, values);
	if (st.error()) {
		values = MFloatVectorArray(1, MFloatVector(feed, kill, 0.0f));
	}
	cerr << "setFeedKillImage values: " << values << endl;
	return m_gsData->solver()->setFeedKillImage(values);
}

MStatus grayScottNode::setDiffImage(MDataBlock &data,
                                    int sampleResolution ) {
	MStatus st = MS::kSuccess;
	if (sampleResolution > 2048) { sampleResolution = 2048; }
	float diffA = data.inputValue(aDiffA).asFloat();
	float diffB = data.inputValue(aDiffB).asFloat();
	MFloatVectorArray values;
	st = sampleTexture(aFeedKillMap, sampleResolution, values);
	if (st.error()) {
		values = MFloatVectorArray(1, MFloatVector(diffA, diffB, 0.0f));
	}
	cerr << "setDiffImage values: " << values << endl;

	return m_gsData->solver()->setDiffImage(values);
}

MStatus grayScottNode::updateMaps(MDataBlock &data) {
	MStatus st;
	JPMDBG;
	int sampleResolution =  data.inputValue(aMapSampleResolution).asInt();
	JPMDBG;
	st = setKernelImage(data, sampleResolution); msert;
	JPMDBG;
	st = setFeedKillImage(data, sampleResolution); msert;
	JPMDBG;
	st = setDiffImage(data, sampleResolution); msert;
	JPMDBG;
	return MS::kSuccess;
}



MStatus grayScottNode::compute( const MPlug &plug, MDataBlock &data ) {

	if ( plug != aOutput) { return ( MS::kUnknownParameter ); }

	MStatus st = MS::kSuccess;

	MTime sT =  data.inputValue( aStartTime).asTime();
	MTime cT =  data.inputValue( aCurrentTime).asTime();
	MTime dT = cT - m_lastTimeIEvaluated;
	m_lastTimeIEvaluated = cT;
	// JPMDBG;
	grayScottNode::MapUpdate updateSchedule = grayScottNode::MapUpdate(
	      data.inputValue(aMapUpdate).asShort());
	// JPMDBG;

	int sampleResolution =  data.inputValue(aMapSampleResolution).asInt();
	// JPMDBG;

	if  (cT == sT ) {
		MString seedFilename = data.inputValue(aSeedImage).asString();
		m_gsData->reset(seedFilename);
		// JPMDBG;
		if (updateSchedule == grayScottNode::kSeedFrame) {
			// JPMDBG;
			st = updateMaps(data); mser;
		}
	}
	else if (dT > MTime(0.0)) {
		// JPMDBG;

		if (m_gsData->valid()) {
			// JPMDBG;

			if (updateSchedule == grayScottNode::kAllFrames) {
				st = updateMaps(data);
			}

			if (! st.error()) {

				float steps = data.inputValue(aSteps).asInt();
				st = m_gsData->solver()->simulate(steps); mser;
			}
		}
	}
	// JPMDBG;
	MDataHandle hOutput = data.outputValue(aOutput);
	MFnPluginData fnOut;
	MTypeId kdid(grayScottData::id);
	// JPMDBG;
	MObject dOut = fnOut.create(kdid , &st ); mser;
	grayScottData *outData = (grayScottData *)fnOut.data(&st); mser;
	// JPMDBG;
	if (m_gsData) {
		*outData = (*m_gsData);
	}
	// JPMDBG;
	MDataHandle outputHandle = data.outputValue(grayScottNode::aOutput, &st ); mser;
	st = outputHandle.set(outData); mser;
	data.setClean( plug );
	// JPMDBG;
	return MS::kSuccess;
}



