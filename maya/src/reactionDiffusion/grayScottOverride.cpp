
#include <maya/MFnDependencyNode.h>
#include <maya/MRenderUtil.h>

#include <maya/MFragmentManager.h>
#include <maya/MShaderManager.h>
#include <maya/MTextureManager.h>

#include <maya/MStateManager.h>
#include <maya/MViewport2Renderer.h>
#include <maya/MFnPluginData.h>


#include "grayScottOverride.h"
#include "grayScottShader.h"
#include "grayScottData.h"


// // Implementation-specific data and helper functions
// namespace
// {
// 	const MString sGrayScottTextureName("simpleNoiseLookupTexture");

// 	const std::vector<float>& GetMayaNoiseTable()
// 	{
// 		// Static so as to only pull the noise data once (it's constant)
// 		static std::vector<float> sNoiseData;
// 		if (sNoiseData.empty())
// 		{
// 			// Pack entire noise table into an array, remapping from
// 			// the [-1,1] range to the [0,1] range. The pixel shader
// 			// will do the inverse operation to extract the real
// 			// data from the texture.
// 			const unsigned int dataSize = MRenderUtil::noiseTableSize();
// 			sNoiseData.resize(dataSize);
// 			for (unsigned int i=0; i<dataSize; i++)
// 			{
// 				sNoiseData[i] = (MRenderUtil::valueInNoiseTable(i) + 1.0f)/2.0f;
// 			}
// 		}
// 		assert(sNoiseData.size() == MRenderUtil::noiseTableSize());
// 		return sNoiseData;
// 	}
// }


////////////////////////////////////////////////////////////////////////////
// Override Implementation
////////////////////////////////////////////////////////////////////////////
MHWRender::MPxShadingNodeOverride *grayScottOverride::creator(
  const MObject &obj)
{
	return new grayScottOverride(obj);
}

grayScottOverride::grayScottOverride(const MObject &obj)
	: MPxShadingNodeOverride(obj)
	, fObject(obj)
	, fFragmentName("")
	, fFileName("")
	, fSamplerState(NULL)
	, fResolvedMapName("")
	, fResolvedSamplerName("")
	, m_solver(0)
{

	static const MString sFragmentName("grayScottPluginFragment");
	const char *sFragmentBody =
#include "grayScottPluginFragment.xml"

	  static const MString sFragmentOutputName("grayScottPluginFragmentOutput");
	const char *sFragmentOutputBody =
#include "grayScottPluginFragmentOutput.xml"

	  static const MString sFragmentGraphName("grayScottPluginGraph");
	const char *sFragmentGraphBody =
#include "grayScottPluginGraph.xml"
	  // Register fragments with the manager if needed
	  //
	  MHWRender::MRenderer * theRenderer = MHWRender::MRenderer::theRenderer();
	if (theRenderer)
	{
		MHWRender::MFragmentManager *fragmentMgr =
		  theRenderer->getFragmentManager();
		if (fragmentMgr)
		{
			// Add fragments if needed
			bool fragAdded = fragmentMgr->hasFragment(sFragmentName);
			bool structAdded = fragmentMgr->hasFragment(sFragmentOutputName);
			bool graphAdded = fragmentMgr->hasFragment(sFragmentGraphName);
			if (!fragAdded)
			{
				fragAdded = (sFragmentName == fragmentMgr->addShadeFragmentFromBuffer(
				               sFragmentBody, false));
			}
			if (!structAdded)
			{
				structAdded = (sFragmentOutputName == fragmentMgr->addShadeFragmentFromBuffer(
				                 sFragmentOutputBody, false));
			}
			if (!graphAdded)
			{
				graphAdded = (sFragmentGraphName == fragmentMgr->addFragmentGraphFromBuffer(
				                sFragmentGraphBody));
			}
			if (fragAdded && structAdded && graphAdded)
			{
				fFragmentName = sFragmentGraphName;
			}
		}
	}
}

grayScottOverride::~grayScottOverride()
{
	MHWRender::MStateManager::releaseSamplerState(fSamplerState);
	fSamplerState = NULL;
}

MHWRender::DrawAPI grayScottOverride::supportedDrawAPIs() const
{
	return MHWRender::kOpenGL | MHWRender::kDirectX11 |
	       MHWRender::kOpenGLCoreProfile;
}

MString grayScottOverride::fragmentName() const
{
	// Reset cached parameter names since the effect is being rebuilt
	fResolvedMapName = "";
	fResolvedSamplerName = "";

	return fFragmentName;
}

void grayScottOverride::getCustomMappings(
  MHWRender::MAttributeParameterMappingList &mappings)
{
	// Set up some mappings for the parameters on the file texture fragment,
	// there is no correspondence to attributes on the node for the texture
	// parameters.
	MHWRender::MAttributeParameterMapping mapMapping( "map", "", false, true);
	mappings.append(mapMapping);
	MHWRender::MAttributeParameterMapping textureSamplerMapping(	"textureSampler",
	    "", false, true);
	mappings.append(textureSamplerMapping);
}

void grayScottOverride::updateDG()
{
	// Pull the file name from the DG for use in updateShader
	MStatus st;

	MFnDependencyNode nodeFn(fObject, &st);
	if (st.error()) { return; }

	MObject dSolverData;
	st = nodeFn.findPlug("grayScottData").getValue(dSolverData);
	if (st.error()) { return; }

	grayScottData *solverData = 0;
	MFnPluginData fnSolverData( dSolverData, &st );
	if (st.error()) { return; }


	solverData = (grayScottData *)fnSolverData.data();
	if (solverData->valid()) {
		m_solver = solverData->solver();
	}
	else {
		m_solver = 0;
	}
}

void grayScottOverride::updateShader(
  MHWRender::MShaderInstance &shader,
  const MHWRender::MAttributeParameterMappingList &mappings)
{
	// Handle resolved name caching
	if (fResolvedMapName.length() == 0)
	{
		const MHWRender::MAttributeParameterMapping *mapping =
		  mappings.findByParameterName("map");
		if (mapping)
		{
			fResolvedMapName = mapping->resolvedParameterName();
		}
	}
	if (fResolvedSamplerName.length() == 0)
	{
		const MHWRender::MAttributeParameterMapping *mapping =
		  mappings.findByParameterName("textureSampler");
		if (mapping)
		{
			fResolvedSamplerName = mapping->resolvedParameterName();
		}
	}


	if (fResolvedMapName.length() > 0 && fResolvedSamplerName.length() > 0)
	{
		// Set sampler to linear-wrap
		if (!fSamplerState)
		{
			MHWRender::MSamplerStateDesc desc;
			desc.filter = MHWRender::MSamplerState::kAnisotropic;
			desc.maxAnisotropy = 16;
			fSamplerState = MHWRender::MStateManager::acquireSamplerState(desc);
		}
		if (fSamplerState)
		{
			shader.setParameter(fResolvedSamplerName, *fSamplerState);
		}

		// Set texture
		MHWRender::MRenderer *renderer = MHWRender::MRenderer::theRenderer();
		if (renderer)
		{
			MHWRender::MTextureManager *textureManager =
			  renderer->getTextureManager();
			if (textureManager)
			{

				const MString sGrayScottTextureName("grayScottTextureName");

				if (m_solver && m_solver->valid()) {
					unsigned width, height;

					const MImage *pImg = m_solver->image();
					pImg->getSize(width, height);


					MHWRender::MTextureDescription desc;
					desc.setToDefault2DTexture();
					desc.fWidth = width;
					desc.fHeight = height;
					desc.fFormat = MHWRender::kR32G32_FLOAT;
					desc.fTextureType = MHWRender::kImage2D;

					desc.fMipmaps = 0;


					MHWRender::MTexture *texture = textureManager->acquireTexture(
					                                 sGrayScottTextureName,
					                                 desc,
					                                 (const void *)pImg->floatPixels(),
					                                 false);


					if (texture)
					{
						MHWRender::MTextureAssignment textureAssignment;
						textureAssignment.texture = texture;
						shader.setParameter(fResolvedMapName, textureAssignment);

						// release our reference now that it is set on the shader
						textureManager->releaseTexture(texture);
					}
				}




			}
		}
	}
}
