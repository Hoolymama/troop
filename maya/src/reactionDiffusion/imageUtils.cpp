#include "imageUtils.h"

float imageUtils::lerp(float s, float e, float t) {return s + (e - s) * t;}

float imageUtils::blerp(float c00, float c10, float c01, float c11,  float tx,
                        float ty) {
	return lerp(lerp(c00, c10, tx), lerp(c01, c11, tx), ty);
}

size_t imageUtils::cellIndex(int x, int y, unsigned w, unsigned h)  {
	unsigned col = ((x + w) % w);
	unsigned row = ((y + h) % h);
	return size_t((row * w) + col);
}

const float *imageUtils::cellVals(int x, int y,  unsigned w,
                                  unsigned h,
                                  const float *pixels)  {
	size_t offset = 2 * cellIndex( x,  y, w, h);
	return &pixels[offset];
}


void imageUtils::nearestSample(float u, float v, const float *pixels ,
                               unsigned w, unsigned h, unsigned channels, float *result ) {
	u = u - floor(u);
	v = v - floor(v);

	size_t rowOffset = (roundf(v * (h - 1))) * w  ;
	size_t colOffset =  roundf(u * (w - 1)) ;
	const float *pixel = pixels + ((rowOffset + colOffset) * 2);
	for (int i = 0; i < channels; ++i)
	{
		result[i] = pixel[i];
	}
}


/*  version with variable channel count */
void imageUtils::bilinearSample(float u, float v, const float *pixels ,
                                unsigned w, unsigned h,  unsigned channels,  float *result ) {
	/* The floor() and modulo stuff is to ensure we always tile */
	u = (u - floor(u)) + 1  ;
	v = (v - floor(v)) + 1  ;

	float xPos =  (u * w) - 0.5;
	int xCell =  floor(xPos);
	float xFrac = xPos - xCell;
	int xCell0 = xCell % w;
	int xCell1 = (xCell + 1) % w;

	float yPos =  (v * h) - 0.5;
	int yCell =  floor(yPos);
	float yFrac = yPos - yCell;
	int yCell0 = yCell % h;
	int yCell1 = (yCell + 1) % h;

	size_t offset_00 = (xCell0 + (yCell0 * w)) * 2 ;
	size_t offset_10 = (xCell1 + (yCell0 * w)) * 2 ;
	size_t offset_01 = (xCell0 + (yCell1 * w)) * 2 ;
	size_t offset_11 = (xCell1 + (yCell1 * w)) * 2 ;

	const float *pixel_00 = pixels + offset_00;
	const float *pixel_10 = pixels + offset_10;
	const float *pixel_01 = pixels + offset_01;
	const float *pixel_11 = pixels + offset_11;

	for (int i = 0; i < channels; ++i)
	{
		result[i] =  blerp(pixel_00[i], pixel_10[i], pixel_01[i], pixel_11[i], xFrac, yFrac);
	}
}

void imageUtils::sample(float u, float v, const float *pixels , unsigned w,
                        unsigned h, unsigned channels, float *result, Interpolation interp )  {
	if (interp == imageUtils::kNearest) {
		nearestSample( u, v, pixels , w,  h, channels, result ) ;
	}
	else {
		bilinearSample( u, v, pixels , w,  h, channels, result ) ;
	}
}

MStatus imageUtils::resize(unsigned newWidth, unsigned newHeight, MImage &image) {

	unsigned oldWidth, oldHeight;
	MStatus st =  image.getSize(oldWidth, oldHeight); msert;
	if (oldWidth == 0 || oldHeight == 0) {
		return MS::kUnknownParameter;
	}

	int channels = image.depth() / 4; // because 4 bytes per float
	float *pixels = image.floatPixels();
	float *buffer = new float[newWidth * newHeight * channels];
	int cell, y, x;
	for ( cell = y = 0; y < newHeight; y ++ ) {
		float v = (y + 0.5) / newHeight;
		for ( x = 0; x < newWidth; x ++, cell ++ ) {
			float u = (x + 0.5) / newWidth;

			float *result = buffer + size_t(cell * channels);
			bilinearSample( u,  v,  pixels , oldWidth,  oldHeight, channels, result );
		}
	}
	image.setFloatPixels( buffer, newWidth, newHeight , channels);

	delete [] buffer;
	buffer = 0;
	return MS::kSuccess;
}
