
#ifndef _grayScottData
#define _grayScottData

#include <maya/MIOStream.h>
#include <string>
#include <vector>
#include <math.h>
#include <maya/MArrayDataHandle.h>
#include <maya/MVector.h>
#include <maya/MObject.h>
#include <maya/MPxData.h>
#include <maya/MTypeId.h>

#include "grayScott.h"



class grayScottData : public MPxData
{
public:
	grayScottData();
	virtual	~grayScottData();
	static	void		*creator();
	virtual	void		copy(const MPxData &);
	grayScottData &operator=(const grayScottData &);
	virtual	MTypeId		typeId() const {return id;}
	static	MTypeId		id;
	virtual	MString		name() const {return typeName;}
	static	MString		typeName;
	MStatus reset(const MString &filename);
	void	clear();
	grayScott 	*solver() const;
	bool valid() const ;
private:
	grayScott 		*m_solver;
};

#endif


