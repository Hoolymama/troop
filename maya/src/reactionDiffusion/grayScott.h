#ifndef _grayScott_hh
#define _grayScott_hh

#include <maya/MIOStream.h>
#include <math.h>
#include <maya/MFloatVector.h>

#include <maya/MFloatVectorArray.h>

#include <maya/MString.h>
#include <maya/MImage.h>
#include "errorMacros.h"
#include "imageUtils.h"

class grayScott
{
public:


	grayScott(const MImage &seed);

	~grayScott();


	MStatus simulate(  int steps );


	bool valid( ) const ;
	MStatus sample(float u, float v,  float *result,
	               imageUtils::Interpolation interp = imageUtils::kBilinear ) const;

	const MImage *image() const;

	// void  clearKernelImage( ) ;
	// void  clearFeedKillImage( ) ;
	// void  clearDiffImage( ) ;

	// MStatus  setKernelImage(const MFloatArray &values,
	//                            unsigned resolution, float angle = 0.0  ) ;

	MStatus setKernelImage(const MFloatArray &values,
	                       const MFloatVector &knTop,
	                       const MFloatVector &knMid,
	                       const MFloatVector &knBot,
	                       float angle = 0
	                      );


	MStatus  setFeedKillImage(const MFloatVectorArray &values ) ;
	MStatus  setDiffImage(const MFloatVectorArray &values ) ;

private:
	enum ImageState {kInvalid, kPixel, kImage};

	ImageState  imageState(const MImage &image ) ;

	MStatus set2ChannelImage(const MFloatVectorArray &values, MImage &image);

	bool  canSimulate() const ;


	bool  hasFullKernelImage( ) ;
	bool  hasFullFeedKillImage( ) ;
	bool  hasFullDiffImage( ) ;

	void laplacian(
	  const float *p, const float *nw, const float *n, const float *ne,
	  const float *w, const float *e, const float *sw, const float *s,
	  const float *se,  const MFloatVector &knTop, const MFloatVector &knMid,
	  const MFloatVector &knBot, float &lapA, float &lapB ) const;

	void laplacian(
	  const float *p, const float *nw, const float *n, const float *ne,
	  const float *w, const float *e, const float *sw, const float *s,
	  const float *se,
	  float k_nw , float k_n  , float k_ne , float k_w  , float k_e  , float k_sw ,
	  float k_s  , float k_se ,
	  float &lapA, float &lapB ) const;

	void laplacian(
	  const float *p, const float *nw, const float *n, const float *ne,
	  const float *w, const float *e, const float *sw, const float *s,
	  const float *se, const float *kernel,  float &lapA, float &lapB ) const;



	MImage m_AB;
	MImage m_dAdB;
	MImage m_kernelImage;
	MImage m_fkImage;
	MImage m_diffImage;

	size_t m_width;
	size_t m_height;

} ;

inline void grayScott::laplacian(
  const float *p, const float *nw, const float *n, const float *ne,
  const float *w, const float *e, const float *sw, const float *s,
  const float *se,  const MFloatVector &knTop, const MFloatVector &knMid,
  const MFloatVector &knBot, float &lapA, float &lapB ) const
{

	lapA = nw[0] * knTop[0] +  n[0] * knTop[1] +  ne[0] * knTop[2]  + w[0] *
	       knMid[0] +  e[0] * knMid[2] + sw[0] * knBot[0] +  s[0] * knBot[1] +  se[0] *
	       knBot[2] - p[0];
	lapB = nw[1] * knTop[0] +  n[1] * knTop[1] +  ne[1] * knTop[2]  + w[1] *
	       knMid[0] +  e[1] * knMid[2] + sw[1] * knBot[0] +  s[1] * knBot[1] +  se[1] *
	       knBot[2] - p[1];

}

inline void grayScott::laplacian(
  const float *p, const float *nw, const float *n, const float *ne,
  const float *w, const float *e, const float *sw, const float *s,
  const float *se,
  float k_nw , float k_n  , float k_ne , float k_w  , float k_e  , float k_sw ,
  float k_s  , float k_se ,
  float &lapA, float &lapB ) const {

	lapA = (nw[0] * k_nw) +  (n[0] * k_n) +  (ne[0] * k_ne)  +
	       ( w[0] * k_w) + (e[0] * k_e) +
	       (sw[0] * k_sw) + (s[0] * k_s) + (se[0] * k_se)
	       - p[0];

	lapB = (nw[1] * k_nw) + (n[1] * k_n) + (ne[1] * k_ne)  +
	       (w[1] * k_w) + (e[1] * k_e) +
	       (sw[1] * k_sw) + (s[1] * k_s) + (se[1] * k_se)
	       - p[1];

}


inline void grayScott::laplacian(
  const float *p, const float *nw, const float *n, const float *ne,
  const float *w, const float *e, const float *sw, const float *s,
  const float *se, const float *kernel,  float &lapA, float &lapB ) const
{

	lapA = (nw[0] * kernel[0]) +  (n[0] * kernel[1]) +  (ne[0] * kernel[2])  +
	       ( w[0] * kernel[3]) + (e[0] * kernel[4]) +
	       (sw[0] * kernel[5]) + (s[0] * kernel[6]) + (se[0] * kernel[7])
	       - p[0];

	lapB = (nw[1] * kernel[0]) + (n[1] * kernel[1]) + (ne[1] * kernel[2])  +
	       (w[1] *  kernel[3]) + (e[1] * kernel[4]) +
	       (sw[1] * kernel[5]) + (s[1] * kernel[6]) + (se[1] * kernel[7])
	       - p[1];

}


#endif


