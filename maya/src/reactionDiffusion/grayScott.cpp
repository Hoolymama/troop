
#include <math.h>
#include <maya/MFloatArray.h>
#include "grayScott.h"
#include "errorMacros.h"
#include "imageUtils.h"


grayScott::grayScott(const MImage &seed)
	: m_AB(),
	  m_dAdB(),
	  m_kernelImage(),
	  m_fkImage(),
	  m_diffImage(),
	  m_width(0),
	  m_height(0)
{
	unsigned int width = 0;
	unsigned int height = 0;
	MStatus st =  seed.getSize(width, height);
	if (st.error() || width == 0 || height == 0) {
		return;
	}
	m_width = width;
	m_height = height;
	unsigned char *seedPixels = seed.pixels();
	m_AB.create(m_width, m_height, 2, MImage::kFloat);
	m_dAdB.create(m_width, m_height, 2, MImage::kFloat);
	float *pixelsAB = m_AB.floatPixels();

	unsigned int cell, x, y;
	for ( cell = y = 0; y < height; y++ ) {
		for ( x = 0; x < width; x++, cell++) {
			size_t offsetA = size_t(cell * 2);
			size_t offsetB = offsetA + 1;
			size_t offsetSeedImage = size_t(cell * 4); // red channel
			pixelsAB[offsetA] =  1.0f;
			pixelsAB[offsetB] = (float)seedPixels[offsetSeedImage] / 255.0f ;
		}
	}
}


bool checkSquare(unsigned num) {
	unsigned root = round(sqrt(num));
	return (root * root) == num;
}

grayScott::~grayScott()
{
	// m_AB.release();
	// m_dAdB.release();
	// m_kernelImage.release();
	// m_fkImage.release();
	// m_diffImage.release();
}

// void  grayScott::clearKernelImage( ) {
// 	m_kernelImage.release();
// }
// void  grayScott::clearFeedKillImage( ) {
// 	m_fkImage.release();
// }
// void  grayScott::clearDiffImage( ) {
// 	m_diffImage.release();
// }





bool grayScott::canSimulate() const  {
	unsigned w, h;
	m_kernelImage.getSize(w, h);
	if (w == 0 || h == 0) {
		return false;
	}
	m_fkImage.getSize(w, h);
	if (w == 0 || h == 0) {
		return false;
	}
	m_diffImage.getSize(w, h);
	if (w == 0 || h == 0) {
		return false;
	}

	return true;
}




MStatus grayScott::set2ChannelImage(const MFloatVectorArray &values,
                                    MImage &image) {
	MStatus st;
	cerr << "set2ChannelImage" << endl;
	// image.release();

	unsigned len  = values.length() ;
	if (! checkSquare(len)) {
		return MS::kUnknownParameter;
	}
	unsigned resolution =  round(sqrt(len));
	JPMDBG;
	st = image.create(resolution, resolution, 2, MImage::kFloat); msert;
	JPMDBG;
	float *pixels =  image.floatPixels();
	for (int i = 0, y = 0; y < resolution; y++) {
		for (int x = 0; x < resolution; x++) {
			int p =  i * 2;
			pixels[p] =  values[i][0]     ;
			pixels[(p + 1)] =  values[i][1] ;
			i++;
		}
	}
	JPMDBG;
	if (resolution != 1) {
		st = imageUtils::resize(m_width, m_height, image); mser;
		return st;
	}
	return MS::kSuccess;
}


MStatus grayScott::setFeedKillImage(const MFloatVectorArray &values)
{
	cerr << "setFeedKillImage" << endl;

	return set2ChannelImage(values, m_fkImage);
}


MStatus grayScott::setDiffImage(const MFloatVectorArray &values)
{
	cerr << "setDiffImage" << endl;

	return set2ChannelImage(values, m_diffImage);
}


/*
The slope texture map has been sampled as a square at low resolution.
This method will create an image, the same resolution as the AB simulation.
It will have 8 float channels, corresponding to the 8 directions of a compass.
(left to right, top to bottom)
Pixels will contain the 8 kernel values, tilted according to the slope values,
which are rotated by angle.

The exception is when the slope values array is empty. Then we make a 1x1
image with the unmodified kernel values in it.

The kernel image is the only place to get kernel values.
*/
MStatus grayScott::setKernelImage(const MFloatArray &values,
                                  const MFloatVector &knTop,
                                  const MFloatVector &knMid,
                                  const MFloatVector &knBot,
                                  float angle
                                 ) {
	cerr << "setKernelImage" << endl;

	MStatus st;
	// m_kernelImage.release();
	unsigned len  = values.length() ;

	if (len == 0) { /* single pixel image */
		cerr << "setKernelImage PIXEL" << endl;

		st = m_kernelImage.create(1, 1, 8, MImage::kFloat); msert;


		unsigned kw, kh;
		m_kernelImage.getSize(kw, kh);
		// cerr << "SIZE " << kw << " X " << kh << " D: " <<  m_kernelImage.depth() << endl;


		float *pixels =  m_kernelImage.floatPixels();

		pixels[0] = knTop[0] ; // north west
		pixels[1] = knTop[1] ; //north
		pixels[2] = knTop[2]  ; // north east
		pixels[3] = knMid[0] ; // west
		pixels[4] = knMid[2]  ; // east
		pixels[5] = knBot[0]  ; // south west
		pixels[6] = knBot[1]   ; // south
		pixels[7] = knBot[2]  ; // south east

		cerr << "pixels[0]" << pixels[0] << endl;


		return  MS::kSuccess;
	}

	cerr << "setKernelImage IMAGE" << endl;

	if (! checkSquare(len)) {
		return MS::kUnknownParameter;
	}
	unsigned resolution =  round(sqrt(len));

	float cs = cos(angle);
	float sn = sin(angle);

	st = m_kernelImage.create(resolution, resolution, 8, MImage::kFloat); msert;
	float *pixels =  m_kernelImage.floatPixels();

	for (int i = 0, y = 0; y < resolution; y++) {
		for (int x = 0; x < resolution; x++) {
			int cell_w = imageUtils::cellIndex( (x - 1),  y,  resolution, resolution);
			int cell_e = imageUtils::cellIndex( (x + 1),  y,  resolution, resolution);
			int cell_n = imageUtils::cellIndex( x,  (y + 1),  resolution, resolution);
			int cell_s = imageUtils::cellIndex( x, ( y - 1),  resolution, resolution);

			float xVal = values[cell_e] - values[cell_w]; // slope x
			float yVal = values[cell_n] - values[cell_s]; // slope y

			// rotate slope
			xVal =  ((xVal * cs) - (yVal * sn)) ;
			yVal =  ((xVal * sn) + (yVal * cs)) ;

			// now put 8 values in the image
			int p =  i * 8;

			pixels[p] = knTop[0] + (-xVal  + yVal); // north west
			pixels[(p + 1)]  = knTop[1] + (yVal); //north
			pixels[(p + 2)] = knTop[2] + (xVal + yVal); // north east
			pixels[(p + 3)]  = knMid[0] + (-xVal); // west
			pixels[(p + 4)] = knMid[2] + (xVal); // east
			pixels[(p + 5)] = knBot[0] + (-xVal  - yVal); // south west
			pixels[(p + 6)] = knBot[1] + (-yVal); // south
			pixels[(p + 7)] = knBot[2] + ( xVal - yVal); // south east
			i++;
		}
	}
	cerr << "setKernelImage B4 Resize" << endl;
	st =  imageUtils::resize(m_width, m_height, m_kernelImage); mser;
	return st;
}

const MImage *grayScott::image() const
{
	return &m_AB;
}

float lerp(float s, float e, float t) {return s + (e - s) * t;}
float blerp(float c00, float c10, float c01, float c11,  float tx,
            float ty) {
	return lerp(lerp(c00, c10, tx), lerp(c01, c11, tx), ty);
}


bool grayScott::valid( ) const  {
	return (
	         m_width > 0 &&
	         m_height > 0 &&
	         m_AB.floatPixels());
}


MStatus  grayScott::sample(float u, float v,  float *result,
                           imageUtils::Interpolation interp) const  {
	if (! valid()) {
		return MS::kFailure;
	}
	const float *pixels = m_AB.floatPixels();
	const unsigned channels = 2;
	imageUtils::sample( u, v, pixels ,  m_width, m_height, channels, result,
	                    interp );
	return MS::kSuccess;
}





grayScott::ImageState  grayScott::imageState(const MImage &image ) {
	unsigned w, h;
	image.getSize(w, h);
	unsigned n =  (w * h) ;
	if (w == m_width && h == m_height ) { return grayScott::kImage; }
	if ((w * h) == 1) { return grayScott::kPixel; }
	return grayScott::kInvalid;
}


MStatus grayScott::simulate(int steps)
{

	const grayScott::ImageState kernelImageState = imageState(m_kernelImage);
	const grayScott::ImageState fkImageState = imageState(m_fkImage);
	const grayScott::ImageState diffImageState = imageState(m_diffImage);

	if ( kernelImageState == grayScott::kInvalid ||
	     fkImageState == grayScott::kInvalid ||
	     diffImageState == grayScott::kInvalid
	   ) { return MS::kUnknownParameter; }

	float *pixelsAB = m_AB.floatPixels();
	float *pixelsdAdB = m_dAdB.floatPixels();
	if (!(pixelsAB && m_width > 0 && m_height > 0)) { return MS::kUnknownParameter; }
	if (steps < 1) { steps = 1; }

	const float *kernelImagePixels = m_kernelImage.floatPixels();
	const float *fkImagePixels = m_fkImage.floatPixels();
	const float *diffImagePixels = m_diffImage.floatPixels();

	bool kernelImageIsImage = kernelImageState == grayScott::kImage;
	bool fkImageIsImage = fkImageState == grayScott::kImage;
	bool diffImageIsImage = diffImageState == grayScott::kImage;

	size_t kernelCellIndex = 0;
	size_t fkCellIndex = 0;
	size_t diffCellIndex = 0;


	cerr << "simulate: "  << endl;


	for (unsigned step = 0; step < steps; step++) {
		for ( int y = 0; y < m_height; y++ ) {
			for (int x = 0; x < m_width; x++  ) {
				const size_t cellIndex = imageUtils::cellIndex(x, y, m_width, m_height);
				const size_t cellIndex2Channel = cellIndex * 2;

				if (kernelImageIsImage) {kernelCellIndex = cellIndex * 8;}
				if (fkImageIsImage) {fkCellIndex = cellIndex2Channel;}
				if (diffImageIsImage) {diffCellIndex = cellIndex2Channel;}

				const float *p =   imageUtils::cellVals(x, y,   m_width, m_height, pixelsAB);
				const float *nw =  imageUtils::cellVals((x - 1), (y + 1), m_width, m_height, pixelsAB);
				const float *n =   imageUtils::cellVals(x, (y + 1), m_width, m_height, pixelsAB);
				const float *ne =  imageUtils::cellVals((x + 1), (y + 1), m_width, m_height, pixelsAB);
				const float *w =   imageUtils::cellVals((x - 1), y, m_width, m_height, pixelsAB);
				const float *e =   imageUtils::cellVals((x + 1), y,  m_width, m_height, pixelsAB);
				const float *sw =  imageUtils::cellVals((x - 1), (y - 1), m_width, m_height, pixelsAB);
				const float *s =   imageUtils::cellVals(x, (y - 1), m_width, m_height, pixelsAB);
				const float *se =  imageUtils::cellVals((x + 1), (y - 1), m_width, m_height, pixelsAB);

				const float *kernel =  &kernelImagePixels[kernelCellIndex] ;
				const float *feedKill =  &fkImagePixels[fkCellIndex] ;
				const float *diff =  &diffImagePixels[diffCellIndex] ;

				float lapA, lapB;
				laplacian(  p, nw, n, ne, w, e, sw, s, se, kernel , lapA, lapB);

				float ABB = p[0] * p[1] * p[1];

				const float &feed = feedKill[0];
				const float &kill = feedKill[1];
				const float &diffA = diff[0];
				const float &diffB = diff[1];

				pixelsdAdB[cellIndex2Channel]  = (diffA * lapA  - ABB + feed *  (1.0 -  p[0]));
				pixelsdAdB[(cellIndex2Channel + 1)] = (diffB * lapB + ABB - (kill + feed) * p[1]) ;
			}
		}
		for (unsigned  y = 0; y < m_height; y++ ) {
			for (unsigned x = 0; x < m_width; x++  ) {
				const  size_t cellIndex2Channel = 2 * imageUtils::cellIndex(x, y, m_width, m_height);
				pixelsAB[cellIndex2Channel] += pixelsdAdB[cellIndex2Channel];
				pixelsAB[(cellIndex2Channel + 1)] += pixelsdAdB[(cellIndex2Channel + 1)];
			}
		}
	}

	cerr << "finished simulate: "  << endl;

	return MS::kSuccess;
}

/*
MStatus grayScott::setKernelImage(const MFloatArray &values,
                                     unsigned resolution, float angle ) {


	float cs = cos(angle);
	float sn = sin(angle);

	MStatus st;
	m_kernelImage.release();
	m_kernelImage.create(resolution, resolution, 2, MImage::kFloat);
	float *pixels =  m_kernelImage.floatPixels();
	for (int i = 0, y = 0; y < resolution; y++) {
		for (int x = 0; x < resolution; x++) {
			int cell_w = imageUtils::cellIndex( (x - 1),  y,  resolution, resolution);
			int cell_e = imageUtils::cellIndex( (x + 1),  y,  resolution, resolution);
			int cell_n = imageUtils::cellIndex( x,  (y + 1),  resolution, resolution);
			int cell_s = imageUtils::cellIndex( x, ( y - 1),  resolution, resolution);


			float xVal = values[cell_e]  - values[cell_w];
			float yVal = values[cell_n] - values[cell_s];


			int p =  i * 2;
			pixels[p] =   ((xVal * cs) - (yVal * sn)) ;
			pixels[(p + 1)] = ((xVal * sn) + (yVal * cs))  ;
			i++;
		}
	}

	st =  imageUtils::resize(m_width, m_height, m_kernelImage);


	if (st.error()) {

		clearKernelImage( );
		return st;
	}

	return MS::kSuccess;
}


*/











// MStatus grayScott::simulateWithKernelSlope(
//   float feed,
//   float kill,
//   float diffA,
//   float diffB,
//   const MFloatVector &knTop,
//   const MFloatVector &knMid,
//   const MFloatVector &knBot,
//   int steps)
// {


// 	float *pixelsAB = m_AB.floatPixels();
// 	float *pixelsdAdB = m_dAdB.floatPixels();
// 	if (!(pixelsAB && m_width > 0 && m_height > 0)) { return MS::kFailure; }
// 	if (steps < 1) { steps = 1; }

// 	unsigned w, h;
// 	m_kernelImage.getSize(w, h);

// 	const float *kernelPixels = m_kernelImage.floatPixels();


// 	for (unsigned step = 0; step < steps; step++) {
// 		for ( int y = 0; y < m_height; y++ ) {
// 			for (int x = 0; x < m_width; x++  ) {

// 				const  size_t cell =  2 * imageUtils::cellIndex(x, y, m_width, m_height);

// 				const float *p =   imageUtils::cellVals(x, y,   m_width, m_height, pixelsAB);
// 				const float *nw =  imageUtils::cellVals((x - 1), (y + 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *n =   imageUtils::cellVals(x, (y + 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *ne =  imageUtils::cellVals((x + 1), (y + 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *w =   imageUtils::cellVals((x - 1), y, m_width, m_height,
// 				                                        pixelsAB);
// 				const float *e =   imageUtils::cellVals((x + 1), y,  m_width, m_height,
// 				                                        pixelsAB);
// 				const float *sw =  imageUtils::cellVals((x - 1), (y - 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *s =   imageUtils::cellVals(x, (y - 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *se =  imageUtils::cellVals((x + 1), (y - 1), m_width, m_height,
// 				                                        pixelsAB);


// 				/*
// 					This calc is happening for every step and is the same result!!!!!
// 					Therefore, we need an 8 channel image to calc once and store the values.
// 				*/
// 				// tilt the convolution kernel
// 				float slopeX = kernelPixels[cell];
// 				float slopeY = kernelPixels[(cell + 1)];

// 				float k_nw = knTop[0] + (-slopeX  + slopeY);
// 				float k_n  = knTop[1] + (slopeY);
// 				float k_ne = knTop[2] + (slopeX + slopeY);

// 				float k_w = knMid[0] + (-slopeX);
// 				float k_e = knMid[2] + (slopeX);

// 				float k_sw = knBot[0] + (-slopeX  - slopeY);
// 				float k_s  = knBot[1] + (-slopeY);
// 				float k_se = knBot[2] + ( slopeX - slopeY);

// 				float lapA, lapB;

// 				laplacian(  p, nw, n, ne, w, e, sw, s, se,  k_nw, k_n, k_ne, k_w, k_e, k_sw,
// 				            k_s, k_se, lapA, lapB);

// 				float ABB = p[0] * p[1] * p[1];

// 				pixelsdAdB[cell]  = (diffA * lapA  - ABB + feed *  (1.0 -  p[0]));
// 				pixelsdAdB[(cell + 1)] = (diffB * lapB + ABB - (kill + feed) * p[1]) ;
// 			}
// 		}
// 		for (unsigned  y = 0; y < m_height; y++ ) {
// 			for (unsigned x = 0; x < m_width; x++  ) {
// 				const  size_t cell = 2 * imageUtils::cellIndex(x, y, m_width, m_height);
// 				pixelsAB[cell] += pixelsdAdB[cell];
// 				pixelsAB[(cell + 1)] += pixelsdAdB[(cell + 1)];

// 			}
// 		}
// 	}
// 	return MS::kSuccess;
// }


// MStatus grayScott::simulateWithKernelSlope(
//   float diffA,
//   float diffB,
//   const MFloatVector &knTop,
//   const MFloatVector &knMid,
//   const MFloatVector &knBot,
//   int steps)
// {

// 	// cerr << "simulateWithKernelSlope" << endl;
// 	float *pixelsAB = m_AB.floatPixels();
// 	float *pixelsdAdB = m_dAdB.floatPixels();
// 	const float *fkPixels = m_fkImage.floatPixels();

// 	if (!(pixelsAB && m_width > 0 && m_height > 0)) { return MS::kFailure; }
// 	if (steps < 1) { steps = 1; }
// 	// cerr << "simulateWithKernelSlope image" << m_width  << " " << m_height <<  endl;


// 	unsigned w, h;
// 	m_kernelImage.getSize(w, h);
// 	cerr << "W x H : " <<  w << " " << h << endl;
// 	const float *kernelPixels = m_kernelImage.floatPixels();


// 	for (unsigned step = 0; step < steps; step++) {
// 		for ( int y = 0; y < m_height; y++ ) {
// 			for (int x = 0; x < m_width; x++  ) {


// 				const  size_t cell =  2 * imageUtils::cellIndex(x, y, m_width, m_height);

// 				const float *p =   imageUtils::cellVals(x, y,   m_width, m_height, pixelsAB);
// 				const float *nw =  imageUtils::cellVals((x - 1), (y + 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *n =   imageUtils::cellVals(x, (y + 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *ne =  imageUtils::cellVals((x + 1), (y + 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *w =   imageUtils::cellVals((x - 1), y, m_width, m_height,
// 				                                        pixelsAB);
// 				const float *e =   imageUtils::cellVals((x + 1), y,  m_width, m_height,
// 				                                        pixelsAB);
// 				const float *sw =  imageUtils::cellVals((x - 1), (y - 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *s =   imageUtils::cellVals(x, (y - 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *se =  imageUtils::cellVals((x + 1), (y - 1), m_width, m_height,
// 				                                        pixelsAB);

// 				// tilt the convolution kernel
// 				float slopeX = kernelPixels[cell];
// 				float slopeY = kernelPixels[(cell + 1)];

// 				float k_nw = knTop[0] + (-slopeX  + slopeY);
// 				float k_n  = knTop[1] + (slopeY);
// 				float k_ne = knTop[2] + (slopeX + slopeY);

// 				float k_w = knMid[0] + (-slopeX);
// 				float k_e = knMid[2] + (slopeX);

// 				float k_sw = knBot[0] + (-slopeX  - slopeY);
// 				float k_s  = knBot[1] + (-slopeY);
// 				float k_se = knBot[2] + ( slopeX - slopeY);

// 				float lapA, lapB;

// 				laplacian(  p, nw, n, ne, w, e, sw, s, se,  k_nw, k_n, k_ne, k_w, k_e, k_sw,
// 				            k_s, k_se, lapA, lapB);

// 				float ABB = p[0] * p[1] * p[1];

// 				const float &feed = fkPixels[cell];
// 				const float &kill = fkPixels[(cell + 1)];

// 				pixelsdAdB[cell]  = (diffA * lapA  - ABB + feed *  (1.0 -  p[0]));
// 				pixelsdAdB[(cell + 1)] = (diffB * lapB + ABB - (kill + feed) * p[1]) ;
// 			}
// 		}
// 		for (unsigned  y = 0; y < m_height; y++ ) {
// 			for (unsigned x = 0; x < m_width; x++  ) {
// 				const  size_t cell = 2 * imageUtils::cellIndex(x, y, m_width, m_height);
// 				pixelsAB[cell] += pixelsdAdB[cell];
// 				pixelsAB[(cell + 1)] += pixelsdAdB[(cell + 1)];

// 			}
// 		}
// 	}
// 	return MS::kSuccess;
// }


// MStatus grayScott::simulateWithNoSlope(
//   float feed,
//   float kill,
//   float diffA,
//   float diffB,
//   const MFloatVector &knTop,
//   const MFloatVector &knMid,
//   const MFloatVector &knBot,
//   int steps)
// {

// 	cerr << "simulateWithNoSlope" << endl;
// 	float *pixelsAB = m_AB.floatPixels();
// 	float *pixelsdAdB = m_dAdB.floatPixels();
// 	if (!(pixelsAB && m_width > 0 && m_height > 0)) { return MS::kFailure; }
// 	if (steps < 1) { steps = 1; }

// 	// const float *kernelPixels = m_kernelImage.floatPixels();


// 	for (unsigned step = 0; step < steps; step++) {
// 		for ( int y = 0; y < m_height; y++ ) {
// 			for (int x = 0; x < m_width; x++  ) {


// 				const  size_t cell =  2 * imageUtils::cellIndex(x, y, m_width, m_height);

// 				const float *p =   imageUtils::cellVals(x, y, m_width, m_height, pixelsAB);
// 				const float *nw =  imageUtils::cellVals((x - 1), (y + 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *n =   imageUtils::cellVals(x, (y + 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *ne =  imageUtils::cellVals((x + 1), (y + 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *w =   imageUtils::cellVals((x - 1), y, m_width, m_height,
// 				                                        pixelsAB);
// 				const float *e =   imageUtils::cellVals((x + 1), y, m_width, m_height,
// 				                                        pixelsAB);
// 				const float *sw =  imageUtils::cellVals((x - 1), (y - 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *s =   imageUtils::cellVals(x, (y - 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *se =  imageUtils::cellVals((x + 1), (y - 1), m_width, m_height,
// 				                                        pixelsAB);


// 				const float &k_nw = knTop[0];
// 				const float &k_n  = knTop[1];
// 				const float &k_ne = knTop[2];
// 				const float &k_w  = knMid[0];
// 				const float &k_e  = knMid[2];
// 				const float &k_sw = knBot[0];
// 				const float &k_s  = knBot[1];
// 				const float &k_se = knBot[2];

// 				float lapA, lapB;

// 				laplacian(  p, nw, n, ne, w, e, sw, s, se,  k_nw, k_n, k_ne, k_w, k_e, k_sw,
// 				            k_s, k_se, lapA, lapB);

// 				float ABB = p[0] * p[1] * p[1];

// 				pixelsdAdB[cell]  = (diffA * lapA  - ABB + feed *  (1.0 -  p[0]));
// 				pixelsdAdB[(cell + 1)] = (diffB * lapB + ABB - (kill + feed) * p[1]) ;
// 			}
// 		}
// 		for (unsigned  y = 0; y < m_height; y++ ) {
// 			for (unsigned x = 0; x < m_width; x++  ) {
// 				const  size_t cell = 2 * imageUtils::cellIndex(x, y, m_width, m_height);
// 				pixelsAB[cell] += pixelsdAdB[cell];
// 				pixelsAB[(cell + 1)] += pixelsdAdB[(cell + 1)];

// 			}
// 		}
// 	}
// 	return MS::kSuccess;
// }


// MStatus grayScott::simulateWithNoSlope(
//   float diffA,
//   float diffB,
//   const MFloatVector &knTop,
//   const MFloatVector &knMid,
//   const MFloatVector &knBot,
//   int steps)
// {

// 	cerr << "simulateWithNoSlope" << endl;
// 	float *pixelsAB = m_AB.floatPixels();
// 	float *pixelsdAdB = m_dAdB.floatPixels();
// 	const float *fkPixels = m_fkImage.floatPixels();

// 	if (!(pixelsAB && m_width > 0 && m_height > 0)) { return MS::kFailure; }
// 	if (steps < 1) { steps = 1; }


// 	for (unsigned step = 0; step < steps; step++) {
// 		for ( int y = 0; y < m_height; y++ ) {
// 			for (int x = 0; x < m_width; x++  ) {


// 				const  size_t cell =  2 * imageUtils::cellIndex(x, y, m_width, m_height);

// 				const float *p =   imageUtils::cellVals(x, y, m_width, m_height, pixelsAB);
// 				const float *nw =  imageUtils::cellVals((x - 1), (y + 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *n =   imageUtils::cellVals(x, (y + 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *ne =  imageUtils::cellVals((x + 1), (y + 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *w =   imageUtils::cellVals((x - 1), y, m_width, m_height,
// 				                                        pixelsAB);
// 				const float *e =   imageUtils::cellVals((x + 1), y, m_width, m_height,
// 				                                        pixelsAB);
// 				const float *sw =  imageUtils::cellVals((x - 1), (y - 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *s =   imageUtils::cellVals(x, (y - 1), m_width, m_height,
// 				                                        pixelsAB);
// 				const float *se =  imageUtils::cellVals((x + 1), (y - 1), m_width, m_height,
// 				                                        pixelsAB);


// 				const float &k_nw = knTop[0];
// 				const float &k_n  = knTop[1];
// 				const float &k_ne = knTop[2];
// 				const float &k_w  = knMid[0];
// 				const float &k_e  = knMid[2];
// 				const float &k_sw = knBot[0];
// 				const float &k_s  = knBot[1];
// 				const float &k_se = knBot[2];

// 				float lapA, lapB;

// 				laplacian(  p, nw, n, ne, w, e, sw, s, se,  k_nw, k_n, k_ne, k_w, k_e, k_sw,
// 				            k_s, k_se, lapA, lapB);

// 				float ABB = p[0] * p[1] * p[1];

// 				const float &feed = fkPixels[cell];
// 				const float &kill = fkPixels[(cell + 1)];

// 				pixelsdAdB[cell]  = (diffA * lapA  - ABB + feed *  (1.0 -  p[0]));
// 				pixelsdAdB[(cell + 1)] = (diffB * lapB + ABB - (kill + feed) * p[1]) ;
// 			}
// 		}
// 		for (unsigned  y = 0; y < m_height; y++ ) {
// 			for (unsigned x = 0; x < m_width; x++  ) {
// 				const  size_t cell = 2 * imageUtils::cellIndex(x, y, m_width, m_height);
// 				pixelsAB[cell] += pixelsdAdB[cell];
// 				pixelsAB[(cell + 1)] += pixelsdAdB[(cell + 1)];

// 			}
// 		}
// 	}
// 	return MS::kSuccess;
// }




