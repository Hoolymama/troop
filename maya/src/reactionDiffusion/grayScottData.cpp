// Author: Julian Mann

#include <maya/MDataHandle.h>
#include "jMayaIds.h"

#include "grayScottData.h"

#define EPSILON 0.0000001

const double bigNum = 99999999999.0  ;

MTypeId grayScottData::id( k_grayScottData );

MString grayScottData::typeName( "grayScottData" );

grayScottData::grayScottData(): m_solver(0) {}

grayScottData::~grayScottData() {
	// clear();
}

void *grayScottData::creator()
{
	return new grayScottData;
}

grayScott 	*grayScottData::solver() const {
	return m_solver;
}
void	grayScottData::clear() {
	// cerr << "grayScottData::clear()" << endl;

	if (m_solver) {
		delete m_solver;
		m_solver = 0;
	}
}


MStatus grayScottData::reset(const MString &filename) {
	// cerr << "grayScottData::reset()" << endl;

	clear();
	MImage img;
	MStatus st = img.readFromFile(filename); msert;
	m_solver = new grayScott(img);
	return st;
}

bool grayScottData::valid( ) const  {
	if (m_solver == 0) {
		return false;
	}
	return m_solver->valid();
}

void grayScottData::copy(const MPxData &otherData)
{
	m_solver = ((const grayScottData &)otherData).solver();
}

grayScottData &grayScottData::operator=(const grayScottData &otherData ) {
	// cerr << "grayScottData::operator=" << endl;
	if (this != &otherData ) {
		m_solver = otherData.solver();
	}
	return *this;
}





