

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MStatus.h>
#include <maya/MFloatVector.h>

class grayScottShader : public MPxNode
{
public:
	grayScottShader();
	virtual           ~grayScottShader();

	virtual MStatus   compute( const MPlug &, MDataBlock & );
	virtual void      postConstructor();

	static  void     *creator();
	static  MStatus   initialize();
	static  MTypeId   id;

private:

	static MObject aGrayScottData;
	static MObject aSampleInterpolation;
	static MObject aUVCoord;
	static MObject aDefaultColor 	;
	static MObject aOutColor;
	static MObject aOutAlpha;


};
