
#ifndef _grayScottNode
#define _grayScottNode

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MTime.h>


#include <maya/MImage.h>

#include "grayScottData.h"

class grayScottNode : public MPxNode
{
public:

	grayScottNode();

	virtual				~grayScottNode();

	virtual MStatus		compute( const MPlug &plug, MDataBlock &data );

	static  void		*creator();

	static  MStatus		initialize();

	static MObject aFeedRate;
	static MObject aKillRate;
	static MObject aDiffA;
	static MObject aDiffB;
	static MObject aSteps;


	static MObject aKernelTop;
	static MObject aKernelMiddle;
	static MObject aKernelBottom;


	static MObject aFeedKillMap;

	static MObject aDiffMap;

	static MObject aKernelSlopeMap;
	static MObject aKernelSlopeAngle;

	static MObject aMapUpdate;
	static MObject aMapSampleResolution;

	static MObject aSeedImage;
	static MObject aCurrentTime;
	static MObject aStartTime;

	static  MObject		aOutput;

	static MTypeId	id;


private:


	enum MapUpdate { kSeedFrame, kAllFrames };
	// enum SlopeRotation { kRotNone, kRot90, kRot180, kRot270,};

	int cellIndex(int x, int y, int xoff, int yoff, int resolution) const;

	MStatus sampleTexture(const MObject &attribute,
	                      int resolution, MFloatArray &result) const;

	MStatus  sampleTexture(const MObject &attribute,
	                       int resolution, MFloatVectorArray &result) const;

	MStatus getTextureName(const MObject &attribute,
	                       MString &name) const;

	MStatus setKernelImage(MDataBlock &data, int sampleResolution ) ;
	MStatus setFeedKillImage(MDataBlock &data,  int sampleResolution ) ;
	MStatus setDiffImage(MDataBlock &data, int sampleResolution ) ;
	MStatus updateMaps(MDataBlock &data) ;


	MTime m_lastTimeIEvaluated;
	grayScottData *m_gsData;

};



inline int grayScottNode::cellIndex(int x, int y, int xoff, int yoff,
                                    int resolution) const {
	unsigned col = ((x + xoff + resolution) % resolution);
	unsigned row = ((y + yoff + resolution) % resolution);
	return int((row * resolution) + col);
}



#endif
