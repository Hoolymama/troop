
#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
#include <maya/MDrawRegistry.h>

#include <errorMacros.h>
// #include <closestPointShader.h>
#include <edgeTreeData.h>
#include <edgeTreeNode.h>
#include <edgeShader.h>

#include <grayScottData.h>
#include <grayScottNode.h>
#include <grayScottShader.h>
#include <differenceMapCmd.h>
#include <grayScottOverride.h>
#include <gradientShader.h>


static const MString sRegistrantId("rdiffPlugin");
static const MString sRegistrantIdFileTex("fileTexturePlugin");
static const MString sRegistrantIdGS("grayScottPlugin");



MStatus initializePlugin( MObject obj )
{
 
	MFnPlugin plugin( obj, PLUGIN_COMPANY, "4.5", "Any");


	plugin.registerNode( "gradientShader", gradientShader::id,
                         gradientShader::creator, gradientShader::initialize,
                         MPxNode::kDependNode, &UserClassifyGeneral );

   return MS::kSuccess;

}

MStatus uninitializePlugin( MObject obj )
{
	MFnPlugin plugin( obj );
	plugin.deregisterNode( gradientShader::id );

	return MS::kSuccess;
}


MStatus initializePlugin( MObject obj )
{

	MStatus st;
	MString method("initializePlugin");

	MFnPlugin plugin( obj, PLUGIN_VENDOR, PLUGIN_VERSION , MAYA_VERSION);

	const MString UserClassify( "texture/3d" );
	const MString UserClassifyGeneral( "utility/general" );
	const MString UserClassifyRdiff("texture/2d:drawdb/shader/texture/2d/rdiff");
 	const MString UserClassifyGS("texture/2d:drawdb/shader/texture/2d/grayScottShader");

	MGlobal::executePythonCommand("import pymel.core;pymel.core.loadPlugin('Kindle')");

 
	st = plugin.registerCommand( "differenceMapCmd", differenceMapCmd::creator ,
	                             differenceMapCmd::newSyntax); mser;

	st = plugin.registerData( "edgeTreeData", edgeTreeData::id,
	                          edgeTreeData::creator ); mser;
	st = plugin.registerNode( "edgeTreeNode", edgeTreeNode::id,
	                          edgeTreeNode::creator, edgeTreeNode::initialize ); mser;
	st = plugin.registerNode( "edgeShader", edgeShader::id, &edgeShader::creator,
	                          &edgeShader::initialize, MPxNode::kDependNode, &UserClassify ); mser;



	st = plugin.registerData( "grayScottData", grayScottData::id,
	                          grayScottData::creator ); mser;
	st = plugin.registerNode( "grayScottNode", grayScottNode::id,
	                          grayScottNode::creator, grayScottNode::initialize ); mser;
	st = plugin.registerNode( "grayScottShader", grayScottShader::id,
	                          &grayScottShader::creator, &grayScottShader::initialize, MPxNode::kDependNode,
	                          &UserClassifyGS ); mser;


	st = plugin.registerNode( "gradientShader", gradientShader::id,
                         gradientShader::creator, gradientShader::initialize,
                         MPxNode::kDependNode, &UserClassifyGeneral ); mser;



	MHWRender::MDrawRegistry::registerShadingNodeOverrideCreator(
	  "drawdb/shader/texture/2d/grayScottShader",
	  sRegistrantIdGS,
	  grayScottOverride::creator);





	const MString
	UserClassifyFile("texture/2d:drawdb/shader/texture/2d/fileTexture");




	MGlobal::executePythonCommand("import troop;troop.load()");

	return st;

}

MStatus uninitializePlugin( MObject obj)
{
	MStatus st;

	MString method("uninitializePlugin");

	MFnPlugin plugin( obj );




	st = plugin.deregisterNode( grayScottShader::id ); mser;
	st = plugin.deregisterNode( grayScottNode::id ); mser;
	st = plugin.deregisterData( grayScottData::id ); mser;
	MHWRender::MDrawRegistry::deregisterShadingNodeOverrideCreator( "drawdb/shader/texture/2d/grayScottShader",
	    sRegistrantIdGS);



	st = plugin.deregisterNode( edgeShader::id ); mser;
	st = plugin.deregisterNode( edgeTreeNode::id ); mser;
	st = plugin.deregisterData( edgeTreeData::id ); mser;


	st = plugin.deregisterCommand( "differenceMapCmd" ); mser;

	// st = plugin.deregisterData( cImgFloatData::id); mser;



	return st;
}






