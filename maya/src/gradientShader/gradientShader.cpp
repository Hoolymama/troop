

#include <maya/MFnPlugin.h>
#include <maya/MPxNode.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFloatVector.h>

#include "errorMacros.h"
#include "jMayaIds.h"

#include "gradientShader.h"
//////////////////////////
// Forward Declarations //
//////////////////////////

class gradientShader;

/////////////////////////////////
// Plugin Lambert Shader Class //
/////////////////////////////////
 

// attribute information

MTypeId gradientShader::id( k_gradientShader );

// input

MObject     gradientShader::aShift;
MObject     gradientShader::aDist;
MObject     gradientShader::aColor;
MObject     gradientShader::aRefPointCamera;
MObject     gradientShader::aUv;

// output

MObject     gradientShader::aOutColor;

gradientShader::gradientShader()
{
}

gradientShader::~gradientShader()
{
}

// creates an instance of the node
void * gradientShader::creator()
{
	return new gradientShader();
}

// initializes attribute information
MStatus gradientShader::initialize()
{
	MStatus st;

	MFnNumericAttribute nAttr; 

	// shift

	aShift = nAttr.create( "shift", "sh", 
						  MFnNumericData::kBoolean, false );
	addAttribute( aShift );

	// distance

	aDist = nAttr.create( "distance", "dis", 
						 MFnNumericData::kFloat, 0.0 );
	addAttribute( aDist );

	// inColor

	aColor = nAttr.createColor( "color", "c" );
	nAttr.setStorable(false);
	addAttribute( aColor );

	// refPointCamera

	aRefPointCamera = nAttr.createPoint( "refPointCamera", "rpc" );
	nAttr.setStorable(false);
	nAttr.setWritable(true);
	nAttr.setRenderSource(true);
	nAttr.setHidden(true);
	addAttribute( aRefPointCamera );

	// uv

	MObject u = nAttr.create( "uCoord", "u", MFnNumericData::kFloat, 0.0 );
	MObject v = nAttr.create( "vCoord", "v", MFnNumericData::kFloat, 0.0 );
	aUv = nAttr.create( "uvCoord", "uv", u, v );
	nAttr.setStorable(false);
	nAttr.setWritable(true);
	nAttr.setRenderSource(true);
	nAttr.setHidden(true);
	addAttribute( aUv );

	// create output attributes here

    aOutColor = nAttr.createColor( "outColor", "oc" );
	PERRORfail(st, "initialize create outColor attribute");
	nAttr.setReadable( true );
	nAttr.setWritable( false );
	addAttribute( aOutColor );
	
	// attribute affects

	attributeAffects ( aShift, aOutColor );
	attributeAffects ( aDist, aOutColor );
	attributeAffects ( aColor, aOutColor );
	attributeAffects ( aRefPointCamera, aOutColor );
	attributeAffects ( aUv, aOutColor );

	// we need to do the followings to cause other input attributes
	// to evaluate with the new values we set

	attributeAffects ( aUv, aUv );
	attributeAffects ( aRefPointCamera, aRefPointCamera );

	return MS::kSuccess;
}


/*

This function gets called by Maya to evaluate the texture.

*/
MStatus gradientShader::compute( const MPlug& plug, MDataBlock& data ) 
{
	MStatus st;

	if ((plug != aOutColor) && (plug.parent() != aOutColor))
		return MS::kUnknownParameter;

	MDataHandle colorH;
	MFloatVector color;

	MDataHandle shiftH = data.inputValue( aShift, &st);
	PERRORfail(st, "compute getting shift attr");
	bool shiftIt = shiftH.asBool();

	MDataHandle distH = data.inputValue( aDist, &st);
	PERRORfail(st, "compute getting distance attr");
	float distance = distH.asFloat();

	MFloatVector clr;

	if ( shiftIt && distance != 0.0 )
	{
		// first evaluate color at default sample posiiton

		clr = data.inputValue( aColor ).asFloatVector();

		// uv is used by 2d textures
		// refPointCamera is used by 3d textures

		MDataHandle refPointCamH = data.inputValue( aRefPointCamera, &st);
		PERRORfail(st, "compute getting refPointCamera attr");
		MFloatVector refPC = refPointCamH.asFloatVector();

		// get current UV

		const float2 & oldUV = data.inputValue(aUv).asFloat2();

		// shift and set the uv/refPointCamera values so
		// we can sample around the current uv/refPointCamera

		MDataHandle outUV = data.outputValue( aUv );
		MDataHandle outPC = data.outputValue( aRefPointCamera );

		outUV.set( oldUV[0]-distance, oldUV[1] );
		outPC.set( refPC.x + distance, refPC.y + distance, refPC.z + distance);
		colorH = data.inputValue( aColor, &st);	// evaluate at new pos
		color = colorH.asFloatVector();
		clr += color;

		outUV.set( oldUV[0]+distance, oldUV[1] );
		outPC.set( refPC.x - distance, refPC.y + distance, refPC.z + distance);
		colorH = data.inputValue( aColor, &st);	// evaluate at new pos
		color = colorH.asFloatVector();
		clr += color;

		outUV.set( oldUV[0], oldUV[1]-distance );
		outPC.set( refPC.x + distance, refPC.y - distance, refPC.z + distance);
		colorH = data.inputValue( aColor, &st);	// evaluate at new pos
		color = colorH.asFloatVector();
		clr += color;

		outUV.set( oldUV[0], oldUV[1]+distance );
		outPC.set( refPC.x - distance, refPC.y - distance, refPC.z + distance);
		colorH = data.inputValue( aColor, &st);	// evaluate at new pos
		color = colorH.asFloatVector();
		clr += color;

		clr /= 5.0;	// average the colors from all locations

		// set sample data back to original values

		outUV.set( oldUV[0], oldUV[1] );
		outPC.set( refPC.x, refPC.y, refPC.z ); 
	}
	else
	{
		colorH = data.inputValue( aColor, &st);
		clr = colorH.asFloatVector();
	}

	MDataHandle outColorHandle = data.outputValue( aOutColor );
	MFloatVector& oclr = outColorHandle.asFloatVector();
	oclr = clr;
	outColorHandle.setClean();

	return MS::kSuccess;
}


//
//
//

MStatus initializePlugin( MObject obj )
{
   const MString UserClassify( "utility/general" );

	MFnPlugin plugin( obj, PLUGIN_COMPANY, "4.5", "Any");
	plugin.registerNode( "gradientShader", gradientShader::id,
                         gradientShader::creator, gradientShader::initialize,
                         MPxNode::kDependNode, &UserClassify );

   return MS::kSuccess;

}

MStatus uninitializePlugin( MObject obj )
{
	MFnPlugin plugin( obj );
	plugin.deregisterNode( gradientShader::id );

	return MS::kSuccess;
}
