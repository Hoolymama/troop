
#ifndef _gradientShader_h
#define _gradientShader_h

//

class gradientShader : public MPxNode
{
	public: // Methods
	gradientShader();
	~gradientShader() override;
	
	MStatus compute( const MPlug&, MDataBlock& ) override;
	
	static  void *          creator();
	static  MStatus         initialize();

	//  Id tag for use with binary file format
	static MTypeId id;

	private:

	float ox, oy, oz;
	int initID, doneID;
	static bool firstTime;

	// Input
	static MObject aShift;
	static MObject aDist;

	static MObject aColor;

	static MObject aRefPointCamera;
	static MObject aUv;

	// Output
	static MObject aOutColor;

};

#endif
