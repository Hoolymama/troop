#ifndef pointOnEdgeInfo_H
#define pointOnEdgeInfo_H

#include <maya/MIOStream.h>
#include <math.h>
#include <maya/MPoint.h>
#include <maya/MVector.h>

#include "errorMacros.h"
#include "mayaMath.h"

using namespace std;


class pointOnEdgeInfo{
	public:

		pointOnEdgeInfo();
		~pointOnEdgeInfo();

		const MFloatPoint & point() const ;
		const float & 	distance() const ;
		const float & 	uCoord() const ;
		const float & 	vCoord() const ;
		const float & 	param() const ;
		bool valid() const;
		// const int & 	id() const ;

		void 	setPoint(const MFloatPoint & rhs) ;

		void 	setDistance(const float & rhs) ;

		void 	setUCoord(const float & rhs) ;

		void 	setVCoord(const float & rhs) ;

		void 	setParam(const float & rhs) ;
		// void 	setId(const int & rhs) ;

	private:

		MFloatPoint m_point;

		float m_distance;

		float m_uCoord;
		
		float m_vCoord;
		
		float m_param;
		
		bool m_valid;

};

#endif

