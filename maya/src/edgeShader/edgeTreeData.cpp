
// Author: Julian Mann

#include "lineKdTree.h"
#include "edgeData.h"
#include "edgeTreeData.h"
#include "errorMacros.h"
// #include "splineVectors.h"
#include "jMayaIds.h"

#include <maya/MDataHandle.h>

#define EPSILON 0.0000001

const double bigNum = 99999999999.0  ;

MTypeId edgeTreeData::id( k_edgeTreeData );
MString edgeTreeData::typeName( "edgeTreeData" );

edgeTreeData::edgeTreeData()
{
	m_pTree = 0;
	
}

edgeTreeData::~edgeTreeData()
{
}

void* edgeTreeData::creator()
{
	return new edgeTreeData;
}


edgeKdTree*	edgeTreeData::tree() const {return m_pTree;}

// clean up
void	edgeTreeData::clear() {
	if (m_pTree) {delete m_pTree; m_pTree = 0;}
}

void edgeTreeData::copy(const MPxData& otherData)
{
	m_pTree = ((const edgeTreeData&)otherData).tree();
}


edgeTreeData& edgeTreeData::operator=(const edgeTreeData & otherData ) {
	if (this != &otherData ) {
		m_pTree = otherData.tree();
	}
	return *this;
}

// create new tree
MStatus edgeTreeData::create(MDataHandle &h) {
	
	MStatus st = MS::kSuccess;
	MString method("edgeTreeData::create" );
	

	clear();
	m_pTree = new edgeKdTree();
	
	if (m_pTree)  {
     	MObject d = h.data();

		if (!(st.error())){
    		st = m_pTree->populate(d);
			if (!(st.error())){
				m_pTree->build();
			} 
		}
 	}
 	return st;
	// NOTE: there is now a lineKdTree which is either
	// null, has zero size, or has size
	// Any calling function should check the state of the tree
}


// 


int edgeTreeData::size(){
	if (m_pTree)
		return m_pTree->size();
	else
		return 0;
}


