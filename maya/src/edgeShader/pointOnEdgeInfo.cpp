#include "pointOnEdgeInfo.h"
#include "errorMacros.h"

pointOnEdgeInfo::pointOnEdgeInfo():m_valid(false){}
pointOnEdgeInfo::~pointOnEdgeInfo() {}

void 	pointOnEdgeInfo::setPoint(const MFloatPoint & rhs) {m_point = rhs; m_valid = true;}
void 	pointOnEdgeInfo::setDistance(const float & rhs) {m_distance = rhs;} 
void 	pointOnEdgeInfo::setUCoord(const float & rhs) {m_uCoord = rhs;} 
void 	pointOnEdgeInfo::setVCoord(const float & rhs) {m_vCoord = rhs;} 
void 	pointOnEdgeInfo::setParam(const float & rhs) {m_param = rhs;} 

const MFloatPoint &  pointOnEdgeInfo::point() const {return m_point; }
const float & 	pointOnEdgeInfo::distance() const {return m_distance; }
const float & 	pointOnEdgeInfo::uCoord() const {return m_uCoord; }
const float & 	pointOnEdgeInfo::vCoord() const {return m_vCoord; }
const float & 	pointOnEdgeInfo::param() const {return m_param; }
bool 	pointOnEdgeInfo::valid() const {return m_valid; }

