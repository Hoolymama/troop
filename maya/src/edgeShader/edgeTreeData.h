/***************************************************************************
                          edgeTreeData.h  -  description
                             -------------------
    begin                : Fri Mar 31 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com
 ***************************************************************************/

#ifndef _edgeTreeData
#define _edgeTreeData

//
// Copyright (C) 2006 hoolyMama
//
// Author: Julian Mann

#include <maya/MIOStream.h>
#include <string>
#include <vector>
#include <math.h>
#include <maya/MArrayDataHandle.h>
#include <maya/MVector.h>
#include <maya/MObject.h>
#include <maya/MPxData.h>
#include <maya/MTypeId.h>

#include "edgeKdTree.h"



class edgeTreeData : public MPxData
{
public:

	edgeTreeData();

	virtual	~edgeTreeData();

	static	void*		creator();

	virtual	void		copy(const MPxData&);

	edgeTreeData& operator=(const edgeTreeData &);

	// type
	virtual	MTypeId		typeId() const {return id;}
	static	MTypeId		id;

	
	virtual	MString		name() const {return typeName;}
	
	static	MString		typeName;

	MStatus	create(MDataHandle &h);

	// void    create(MArrayDataHandle &ah);
	
	// void create( MArrayDataHandle &ah, 
	//   const MObject & childGeomPlug,
	//   const MObject & childActivePlug,
	//   const MObject & childTangentPlug,
	//   const MObject & childNormalPlug,
	//   const MObject & childCircularPlug,
	//   MVectorArray & forces 
	//   ) ;
	
	void	clear();

	edgeKdTree *	tree() const;
	
	// MStatus closestPointOnLine(
	//  const MPoint &searchPoint,
	//  MVector &resultPoint,
	//  double &resultU,
	//  double &resultParam
 //   ) const;
	
	
	// MStatus closestPointOnLine(const MPoint &searchPoint,
	// 						   MPoint &resultPoint,
	// 						   MVector &resultTangent,
	// 						   MVector &resultVelocity,
	// 						   double &resultU,
	// 						   double &resultParam
	// 						   ) const;
	
	// bool closestPointOnLineInRadius(
	// 								const MPoint &searchPoint,
	// 								double rad,
	// 								edgeData & result 
	// 								)  const;
	
	// MStatus closestPointOnLineInRadius(
	// 								   const MPoint &searchPoint,
	// 								   double radius,
	// 								   MPoint &resultPoint,
	// 								   MVector &resultTangent,
	// 								   MVector &resultVelocity,
	// 								   double &resultU,
	// 								   double &resultDist
	// 								   )  const ;
	
	
 //   MStatus closestPointOnLine(
 //       const MVector &searchPoint,
 //       MVector &resultPoint,
 //       double &resultU,
 //       double &resultParam
 //   ) const ;
 //   MStatus closestPointOnLine(
 //       const MVector &searchPoint,
 //       MVector &resultPoint
 //   ) const ;
	// MStatus closestPointsOnLine(
	//     const MVectorArray &searchPoints,
	//     MVectorArray &resultPoints,
	//     MDoubleArray &resultUs,
	//     //MIntArray &resultIds,
	//     MDoubleArray &resultParams
	// ) const ;

 //    MStatus closestPointsOnLine(
 //       const MVectorArray &searchPoints,
 //       MVectorArray &resultPoints
 //   ) const ;
	
	// MStatus closestTangentsOnLine(
	// 	const MVectorArray &searchPoints,
	// 	MVectorArray &resultTangents
	//   ) const;
	
	// MStatus closestTangentsOnLine(
	// const MVectorArray &searchPoints,
	// const MVectorArray &forces,
	// MVectorArray &resultTangents
	// ) const;
	
	// MStatus blendClosestTangentsOnLine(
	//  const MVectorArray &searchPoints,
	//  double distance,
	//  double attenuation,
	//  MVectorArray &resultTangents
	//  ) const;
	
	// MStatus blendClosestTangentsOnLine(
	//  const MVectorArray &searchPoints,
	//  const MVectorArray &forces,
	//  double distance,
	//  double attenuation,
	//  MVectorArray &resultTangents
	//  ) const ;
	
	
	// MStatus closestTangentsOnLine(
	// const MFloatVectorArray &searchPoints,
	// const MVectorArray &forces,
	// double distance,
	// double attenuation,
	// MFloatPointArray &resultTangents,
	// MIntArray &affected
	// ) const;
	
	
   int size();

private:

	edgeKdTree 	*	m_pTree;

};

#endif


