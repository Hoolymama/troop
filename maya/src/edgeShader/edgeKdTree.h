#ifndef _edgeKdTree
#define _edgeKdTree

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MString.h>
#include <maya/MVector.h>
#include <maya/MIntArray.h>
#include <maya/MFnDynSweptGeometryData.h>
#include <maya/MDynSweptTriangle.h>
#include <maya/MItMeshPolygon.h>

#include "edgeData.h"
#include "errorMacros.h"


class     edgeData ;

typedef vector<edgeData*> EDGE_LIST;


struct edgeDataKdNode  {
	bool bucket;							/// is this node a bucket
	axis cutAxis;							/// if not, this axis will divide it 
	double cutVal;							/// at this value
	edgeDataKdNode *loChild, *hiChild;		/// and these pointers point to the children
	unsigned int loPoint, hiPoint;			/// Indices into the permutations array
	EDGE_LIST * overlapList;				/// list of edgeDatas whose boumding boxes intersect this bucket

	
};


class edgeKdTree 
{
	public:

		edgeKdTree();			

		~edgeKdTree();  
		const edgeDataKdNode * root();
		edgeKdTree& operator=(const edgeKdTree & otherData );

		void 		build(); 							/// build root from m_perm

		edgeDataKdNode * 	build( int low,  int high ); 	/// build children recursively
		
		int size() ;

		//MStatus 	getSize(int &siz);  			/// size of m_perm and _prismVector - returns failure if they are different

		void  	setMaxPointsPerBucket( int b) {if (b < 1) {m_maxPointsPerBucket = 1;} else {m_maxPointsPerBucket = b;}}

		MStatus 	init();

		// MStatus populate(const MFnDynSweptGeometryData &g, int id) ;
		
		MStatus populate( MObject &d) ;
		
		void wirthSelect(  int left,  int right,  int k, axis cutAxis )  ;

		void makeEmpty() ; 
		
		void makeEmpty(edgeDataKdNode * p) ; 

		void  setOverlapList(edgeDataKdNode * p,  edgeData * sph  );
		
		EDGE_LIST & edgeList();

  	// void closestEdge(
  	// 	const edgeDataKdNode * p,
  	// 	const MPoint &searchPoint,  
  	// 	double & radius, 
  	// 	 edgeData &  result )  const ;

	  	void closestEdge(
  		const edgeDataKdNode * p,
  		const MFloatPoint &searchPoint,  
  		float & radius, 
  		pointOnEdgeInfo &  result 
  		)  const ;


	private:	
		axis 		findMaxAxis(const  int low, const  int  high) const;
		// void  searchList(const EDGE_LIST * overlapList,const MPoint &searchPoint, double & radius, edgeData & result) const   ;
		void  searchList(
		const EDGE_LIST * overlapList,
		const MFloatPoint &searchPoint,
		float & radius,
		pointOnEdgeInfo & result  
		) const ;

		EDGE_LIST * m_perm;				/// pointer to (permutations) - a list of pointers to elements of _edgeDataVector
		edgeDataKdNode * m_pRoot;		/// pointer to root of tree
		int m_maxPointsPerBucket;		/// at build time keep recursing until no bucket holds more than this
		
} ;

#endif


