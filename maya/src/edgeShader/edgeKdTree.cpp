
/***************************************************************************
                          edgeKdTree.cpp  -  description

    begin                : Mon Apr 3 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com
 ***************************************************************************/

#define P_TOLERANCE 0.000001
#include <maya/MItMeshEdge.h>
#include <maya/MItMeshVertex.h>

#include "edgeKdTree.h"

edgeKdTree::edgeKdTree() //  constructor
	: m_perm(0),
	  m_pRoot(0),
	  m_maxPointsPerBucket(4)
{
	m_pRoot = 0;
	m_perm = new EDGE_LIST;

}

edgeKdTree::~edgeKdTree()
{
	if (m_perm) {
		EDGE_LIST::iterator p =  m_perm->begin();
		while (p != m_perm->end()) {
			delete *p;
			*p = 0;
			p++;
		}
		delete m_perm;
		m_perm = 0;
	}
	makeEmpty(m_pRoot);  // recursively delete tree
}

void edgeKdTree::makeEmpty(edgeDataKdNode *p) {
	if (p) { // if the pointer aint NULL
		if (!(p->bucket)) {
			makeEmpty(p->loChild);	// recurse through kids
			makeEmpty(p->hiChild);
		}
		else {
			delete p->overlapList;
			p->overlapList  = 0;
		}
		delete p;
		p = 0;					// zap the little bugger
	}
}

const edgeDataKdNode *edgeKdTree::root() {
	return m_pRoot;
};

//populate the tree with edgeData objects
MStatus edgeKdTree::populate( MObject &d)  {
	MStatus st;
	MItMeshEdge edgeIter(d, &st); msert;
	MItMeshVertex vertexIter(d, &st); msert;
	edgeIter.reset();
	for (; ! edgeIter.isDone(); edgeIter.next()) {
		MFloatPoint p0 = MFloatPoint(edgeIter.point(0, MSpace::kWorld, &st));
		MFloatPoint p1 = MFloatPoint(edgeIter.point(1, MSpace::kWorld, &st));

		if (edgeIter.onBoundary()) {
			int vertexIndex0 = edgeIter.index(0);
			int vertexIndex1 = edgeIter.index(1);
			float2 uvPoint0, uvPoint1;
			int prevIndex;
			vertexIter.setIndex(vertexIndex0, prevIndex);
			vertexIter.getUV(uvPoint0);
			vertexIter.setIndex(vertexIndex1, prevIndex);
			vertexIter.getUV(uvPoint1);
			MFloatArray uvStartVals(uvPoint0, 2);
			MFloatArray uvEndVals(uvPoint1, 2);


			edgeData *t = new edgeData(p0, p1, uvStartVals, uvEndVals);
			m_perm->push_back(t) ;
		}
	}

	return MS::kSuccess;
}

int edgeKdTree::size() {
	return int(m_perm->size());
}

void edgeKdTree::build() {
	int low = 0;
	int high = (size() - 1);
	m_pRoot = build(low, high);

	EDGE_LIST::iterator currentBox = m_perm->begin();
	while (currentBox != m_perm->end()) {
		setOverlapList(m_pRoot , *currentBox);
		currentBox++;
	}
}


edgeDataKdNode   *edgeKdTree::build( int low,  int high	) {
	// // cout << "in subBuild routine " << endl;
	edgeDataKdNode *p = new edgeDataKdNode;

	if (((high - low) + 1) <= m_maxPointsPerBucket) {
		// only bucket nodes will hold an overlapList
		p->bucket = true;
		p->loPoint = low;
		p->hiPoint = high;
		p->loChild = 0;
		p->hiChild = 0;
		p->overlapList = new EDGE_LIST;
	}
	else {

		p->bucket = false;
		p->cutAxis = findMaxAxis(low, high);
		int mid = ((low + high) / 2);
		wirthSelect(low, high, mid, p->cutAxis);
		p->cutVal = ((*m_perm)[mid])->center(p->cutAxis);
		p->loChild = build(low, mid);
		p->hiChild = build(mid + 1, high);
	}
	return p;
}


void edgeKdTree::wirthSelect(  int left,  int right,  int k, axis cutAxis )
{
	int n = (right - left) + 1;
	if (n <= 1) { return; }
	int i, j, l, m;
	edgeData *x;
	edgeData *tmp;

	l = left;
	m = right;
	while (l < m) {
		x = (*m_perm)[k];
		i = l;
		j = m;
		do {
			while (  ((*m_perm)[i])->center(cutAxis) <  x->center(cutAxis)  ) { i++; }
			while (  ((*m_perm)[j])->center(cutAxis) >  x->center(cutAxis)  ) { j--; }

			if (i <= j) {
				// swap
				tmp = (*m_perm)[i];
				(*m_perm)[i] = (*m_perm)[j] ;
				(*m_perm)[j] = tmp ;
				i++; j--;
			}
		}
		while (i <= j);
		if (j < k) { l = i; }
		if (k < i) { m = j; }
	}
}

axis edgeKdTree::findMaxAxis(const  int low, const  int high) const {


	// The idea here is just to find the axis containing the longest
	// side of the bounding rectangle of the points

	// From a vector of N points we just take sqrtN samples
	// in order to keep the time down to O(N)
	// should be ok though
	double minx , miny,  minz , maxx , maxy , maxz, tmpVal;
	double sx, sy, sz;

	int  num = (high - low ) + 1;
	int interval = int(sqrt(double(num)));
	// unsigned int intervalSq = interval*interval;
	int i;
	MPoint p = ((*m_perm)[low])->center();
	minx = p.x;
	maxx = minx;
	miny = p.y;
	maxy = miny;
	minz = p.z;
	maxz = minz;

	for (i = (low + interval); i <= high; i += interval ) {
		p = ((*m_perm)[i])->center();
		tmpVal = p.x;
		if (tmpVal < minx) {
			minx = tmpVal;
		}
		else {
			if (tmpVal > maxx) {
				maxx = tmpVal;
			}
		}
		tmpVal = p.y;
		if (tmpVal < miny) {
			miny = tmpVal;
		}
		else {
			if (tmpVal > maxy) {
				maxy = tmpVal;
			}
		}
		tmpVal = p.z;
		if (tmpVal < minz) {
			minz = tmpVal;
		}
		else {
			if (tmpVal > maxz) {
				maxz = tmpVal;
			}
		}
	}
	sx = maxx - minx;
	sy = maxy - miny;
	sz = maxz - minz;

	if (sx > sy) {
		// y is not the greatest
		if (sx > sz) {
			return mayaMath::xAxis;
		}
		else {
			return mayaMath::zAxis;
		}
	}
	else {
		// x is not the greatest
		if (sy > sz) {
			return mayaMath::yAxis;
		}
		else {
			return mayaMath::zAxis;
		}
	}
}

void  edgeKdTree::setOverlapList(edgeDataKdNode *p,  edgeData *tb  )  {
	// recursive setOverlapList method
	// put lineBox in every bucket it overlaps
	if (p->bucket) {
		p->overlapList->push_back(tb);
	}
	else {
		if (tb->min(p->cutAxis) < p->cutVal) {
			setOverlapList(p->loChild, tb);
		}
		if (tb->max(p->cutAxis) > p->cutVal) {
			setOverlapList(p->hiChild, tb);
		}
	}
}


void  edgeKdTree::searchList(
  const EDGE_LIST *overlapList,
  const MFloatPoint &searchPoint,
  float &radius,
  pointOnEdgeInfo &result
) const {
	EDGE_LIST::const_iterator curr;
	curr = overlapList->begin();
	while (curr != overlapList->end()) {
		if ((*curr)->sphereIntersectsBB(searchPoint, radius)  ) {
			//double dist = 0;
			if ( (*curr)->sphereIntersectsLine(searchPoint, radius, result)) {
				radius	=  result.distance();
			}
		}
		curr++;
	}
}

// recursive function to find the closest line
// radius is passed as reference but is not const.
// This is how we shrink it during the search.
void edgeKdTree::closestEdge(
  const edgeDataKdNode *p,
  const MFloatPoint &searchPoint,
  float &radius,
  pointOnEdgeInfo   &result  )  const  {

	if (p->bucket) {
		searchList( p->overlapList, searchPoint, radius, result);
	}
	else {
		double diff = searchPoint[(p->cutAxis)] -
		              p->cutVal; // distance to the cut wall for this bucket
		if (diff < 0) { // we are in the lo child so search it
			closestEdge(p->loChild, searchPoint, radius, result);
			if (radius >= -diff) { // if radius overlaps the hi child then search that too
				closestEdge(p->hiChild, searchPoint, radius, result);
			}
		}
		else {   // we are in the hi child so search it
			closestEdge(p->hiChild, searchPoint, radius, result);
			if (radius >= diff) { // if radius overlaps the lo child then search that too
				closestEdge(p->loChild, searchPoint, radius, result);
			}
		}
	}
}

EDGE_LIST &edgeKdTree::edgeList() {
	return *m_perm;
}

