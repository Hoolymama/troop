

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MStatus.h>
#include <maya/MFloatVector.h>

class edgeShader : public MPxNode
{
	public:
		edgeShader();
		virtual           ~edgeShader();
		
		virtual MStatus   compute( const MPlug&, MDataBlock& ) override;
		virtual void      postConstructor() override;
		SchedulingType schedulingType() const override { return SchedulingType::kParallel; }

		static  void *    creator();
		static  MStatus   initialize();
		static  MTypeId   id;
		
	private:
		static MObject aMaxDistance;
		static MObject aPlaceMat;
		static MObject aPointWorld;
		static MObject aRefPointWorld;
		// static MObject aUVCoord;
		static MObject aEdgeTree; 
		static MObject aDefaultColor 	;			
		static MObject aOutColor;	
		static MObject aOutAlpha;	
		
};
