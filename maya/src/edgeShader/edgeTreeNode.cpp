/***************************************************************************
ptTreeNode.cpp  -  description
-------------------
    begin                : Mon Apr 3 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com

    A node to store an array of points in a kdTree and make the tree data available in the dependency graph
	***************************************************************************/

#include <maya/MFnTypedAttribute.h>
#include <maya/MFnPluginData.h>

#include "errorMacros.h"
#include "edgeTreeData.h"
#include "edgeTreeNode.h"
#include "jMayaIds.h"


MTypeId     edgeTreeNode::id( k_edgeTreeNode );

//  stuff
//////////////////////////////////////////
MObject     edgeTreeNode::aInput;
MObject 	edgeTreeNode::aOutput;


edgeTreeNode::edgeTreeNode() {
	m_pd = new edgeTreeData;
}

edgeTreeNode::~edgeTreeNode() {

	if (m_pd) { delete m_pd; }

	m_pd = 0;
}

void *edgeTreeNode::creator()
{
	return new edgeTreeNode();
}

MStatus edgeTreeNode::initialize()

//
{
	MStatus st;

	MFnTypedAttribute tAttr;

	// GEOMETRY
	aInput = tAttr.create("input", "in", MFnData::kMesh);
	tAttr.setReadable(false);
	st = addAttribute( aInput ); mser

	aOutput = tAttr.create("output", "out", edgeTreeData::id);
	tAttr.setReadable(true);
	tAttr.setStorable(false);
	tAttr.setCached(false);

	addAttribute(aOutput);

	st = attributeAffects(aInput, aOutput ); mser;

	return MS::kSuccess;
}


MStatus edgeTreeNode::compute( const MPlug &plug, MDataBlock &data ) {

	if ( plug != aOutput) { return ( MS::kUnknownParameter ); }

	MStatus st;

	MDataHandle hIn = data.inputValue( aInput );

	m_pd->create(hIn);

	////////////////////////////////////////////////////////////////

	MDataHandle hOutput = data.outputValue(aOutput);
	MFnPluginData fnOut;
	MTypeId kdid(edgeTreeData::id);

	MObject dOut = fnOut.create(kdid , &st ); mser;
	edgeTreeData *outTree = (edgeTreeData *)fnOut.data(&st); mser;

	if (m_pd) {
		*outTree = (*m_pd);
	}

	MDataHandle outputHandle = data.outputValue(edgeTreeNode::aOutput, &st ); mser;
	st = outputHandle.set(outTree); mser;
	data.setClean( plug );

	return MS::kSuccess;
}



