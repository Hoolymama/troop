/***************************************************************************
edgeData.cpp  -  description
-------------------
    begin                : Mon Apr 3 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com

	a class to store lines in a bounding box for use in a lineKdTree.
	***************************************************************************/

#include "edgeData.h"
#include "errorMacros.h"

const double TIMESTEP = 1.0f / 24.0f;
const double EPSILON = 0.0000001;
//const double NEG_EPSILON = -0.0001;

edgeData::edgeData(){}

edgeData::edgeData( 		
	const MFloatPoint &startPoint,
	const MFloatPoint &endPoint,
	const MFloatArray &uvStart,
	const MFloatArray &uvEnd
	// int i
):
	m_startPoint(startPoint),
	m_endPoint(endPoint),
	m_uvStart(uvStart),
	m_uvEnd(uvEnd)
	// m_id(i)
{
	computeBoundingBox();
}

edgeData::~edgeData(){}

edgeData& edgeData::operator=(const edgeData& other) {
	if (this != &other) {
		m_startPoint = other.startPoint();
		m_endPoint = other.endPoint();
		m_uvStart = other.uvStart();
		m_uvEnd = other.uvEnd();
		m_box = other.box();
		// m_id = other.id();
	}
	return *this;
}

void edgeData::computeBoundingBox(){
	m_box = MBoundingBox(MPoint(m_startPoint),MPoint(m_endPoint));
	if (
		(m_box.width() < EPSILON) ||
		(m_box.height() < EPSILON) ||
		(m_box.depth() < EPSILON) 
		){
		m_box.expand(m_box.max() + MVector(0.0001,0.0001,0.0001));
		m_box.expand(m_box.min() - MVector(0.0001,0.0001,0.0001));
	}	
}

double  edgeData::min(axis a) const {return m_box.min()[a];}

double  edgeData::max(axis a) const {return m_box.max()[a];}

MVector edgeData::center() const {return m_box.center();}

double edgeData::center(axis a) const {return m_box.center()[a];}


// int edgeData::id() const { return m_id;}

const MFloatPoint & edgeData::startPoint() const {return m_startPoint;}

const MFloatPoint & edgeData::endPoint() const {return m_endPoint;}

const MBoundingBox & edgeData::box() const {return m_box;}

const MFloatArray & edgeData::uvStart() const {return m_uvStart;}

const MFloatArray & edgeData::uvEnd() const {return m_uvEnd;}

bool edgeData::sphereIntersectsBB(const MFloatPoint &c, float r) const  {
	return mayaMath::sphereBoxIntersection( m_box, c, r);
}

bool edgeData::sphereIntersectsLine(const MFloatPoint &c, float r,  pointOnEdgeInfo & pInfo) const {
	float param;
	MFloatPoint pt  = mayaMath::closestPointOnLine(c, m_startPoint, m_endPoint, param );
	MFloatVector v = pt - c;
	double sqDist = (v*v);
	if ((sqDist) <= (r*r)) { 
		pInfo.setPoint(pt);
		pInfo.setDistance(sqrt(sqDist));
		pInfo.setUCoord(m_uvStart[0] + (param * (m_uvEnd[0] - m_uvStart[0])));
		pInfo.setUCoord(m_uvStart[1] + (param * (m_uvEnd[1] - m_uvStart[1])));
		pInfo.setParam(param);
		// pInfo.setId(m_id);
		return true;
	}
	return false;
}

// const MVector & edgeData::tangent() const{
// 	return m_tangent;
// }
// const double & edgeData::length() const{
// 	return m_length;
// }

