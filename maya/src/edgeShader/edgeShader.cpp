#include <maya/MIOStream.h>
#include <math.h>
#include <maya/MPxNode.h>
#include <maya/MStatus.h>

#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnPluginData.h>
#include <maya/MFloatVector.h>
#include <maya/MDoubleArray.h>
#include <maya/MVector.h>
#include <maya/MPointArray.h>
#include <maya/MFloatPoint.h>
#include <maya/MRampAttribute.h>
#include <maya/MVectorArray.h>
#include <maya/MTime.h>

#include "errorMacros.h"
#include "jMayaIds.h"

#include "edgeTreeData.h"

#include "pointOnEdgeInfo.h"

#include "edgeShader.h"


MObject edgeShader::aMaxDistance;
MObject edgeShader::aPlaceMat;
MObject edgeShader::aPointWorld;
MObject edgeShader::aRefPointWorld;
MObject edgeShader::aEdgeTree;
MObject edgeShader::aDefaultColor 	;
MObject edgeShader::aOutColor;
MObject edgeShader::aOutAlpha;


MTypeId edgeShader::id( k_edgeShader );


edgeShader::edgeShader()
{
}

edgeShader::~edgeShader()
{
}


void *edgeShader::creator()
{
	return new edgeShader();
}
void edgeShader::postConstructor( )
{
	// setMPSafe(false);
}


MStatus edgeShader::initialize()
{
	MStatus st;

	MString method("edgeShader::initialize");
	MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnUnitAttribute uAttr;

	aEdgeTree = tAttr.create("edgeTreeNode", "edt", edgeTreeData::id ) ;
	tAttr.setStorable(false);
	tAttr.setCached(false);
	st = addAttribute( aEdgeTree ); mser;


	aMaxDistance = nAttr.create("maxDistance", "mxd", MFnNumericData::kDouble, 1.0);
	nAttr.setKeyable(true);
	nAttr.setHidden(false);
	st = addAttribute(aMaxDistance); mser;


	aPlaceMat = mAttr.create("placementMatrix", "pm", MFnMatrixAttribute::kFloat);
	mAttr.setKeyable(true);
	mAttr.setStorable(true);
	mAttr.setReadable(true);
	mAttr.setWritable(true);
	st = addAttribute(aPlaceMat); mser;

	aPointWorld = nAttr.createPoint("pointWorld", "pw");
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setReadable(true);
	nAttr.setWritable(true);
	nAttr.setHidden(true);
	st = addAttribute(aPointWorld); mser;

	aRefPointWorld = nAttr.createPoint("refPointWorld", "rpw");
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setReadable(true);
	nAttr.setWritable(true);
	nAttr.setHidden(true);
	st = addAttribute(aRefPointWorld); mser;

	aDefaultColor = nAttr.createColor( "defaultColor", "dc" );
	nAttr.setDefault(1.0f, 0.0f, 0.0f);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setUsedAsColor(true);
	nAttr.setReadable(true);
	nAttr.setWritable(true);
	st = addAttribute( aDefaultColor ); mser;

	aOutColor = nAttr.createColor( "outColor", "oc" );
	nAttr.setWritable( false );
	nAttr.setKeyable( false );
	nAttr.setReadable( true);
	nAttr.setStorable( false);
	st = addAttribute( aOutColor ); mser;


	aOutAlpha = nAttr.create( "outAlpha", "oa", MFnNumericData::kFloat);
	nAttr.setKeyable(false);
	nAttr.setStorable(false);
	nAttr.setReadable(true);
	nAttr.setWritable(false);
	addAttribute(aOutAlpha);


	attributeAffects (aPlaceMat, aOutColor);
	attributeAffects (aPointWorld, aOutColor);
	attributeAffects (aRefPointWorld, aOutColor);
	attributeAffects (aEdgeTree, aOutColor);
	attributeAffects (aDefaultColor, aOutColor);
	attributeAffects (aMaxDistance, aOutColor);

	attributeAffects (aPlaceMat, aOutAlpha);
	attributeAffects (aPointWorld, aOutAlpha);
	attributeAffects (aRefPointWorld, aOutAlpha);
	attributeAffects (aEdgeTree, aOutAlpha);
	attributeAffects (aDefaultColor, aOutAlpha);
	attributeAffects (aMaxDistance, aOutAlpha);

	return MS::kSuccess;
}


MStatus edgeShader::compute( const MPlug      &plug,  MDataBlock &data )
{
	MStatus st;

	MString method("edgeShader::compute");

	if ((plug != aOutColor) && (plug.parent() != aOutColor) && (plug != aOutAlpha)) 	{ return MS::kUnknownParameter; }

	MFloatVector &defaultColor = data.inputValue(aDefaultColor).asFloatVector();

	MFloatVector resultColor = defaultColor;
	float resultAlpha = 0.0f;

	double maxDist = data.inputValue(aMaxDistance).asDouble();
	if (maxDist <= 0.0) 	{ return MS::kUnknownParameter; }
	float3 &worldPos = data.inputValue(aPointWorld).asFloat3();
	float3 &refWorldPos = data.inputValue(aRefPointWorld).asFloat3();

	MFloatMatrix &mat = data.inputValue(aPlaceMat).asFloatMatrix();

	MFloatPoint pos(worldPos[0], worldPos[1], worldPos[2]);
	MFloatPoint rwpos(refWorldPos[0], refWorldPos[1], refWorldPos[2]);

	// sample point in world
	pos *= mat;


	// lookup closest edge in a kdTree of lines
	edgeKdTree 	*tree = 0;
	edgeTreeData *edgeTree = 0;

	MDataHandle hEdgeTree = data.inputValue(aEdgeTree, &st); msert;
	MObject dEdgeTree = hEdgeTree.data();
	MFnPluginData fnEdgeTree( dEdgeTree, &st );
	if (!(st.error())) {
		edgeTree = (edgeTreeData *)fnEdgeTree.data();
		if (edgeTree->size())  { tree = edgeTree->tree(); }
	}
	if (!tree) { return MS::kUnknownParameter; }

	// pointOnEdgeInfo - an opject containing the result of the lookup
	// Contains:
	// point() const ;
	// distance() const ;
	// uCoord() const ;
	// vCoord() const ;
	// param() const ;

	pointOnEdgeInfo pInfo;
	float dist = float(maxDist);
	tree->closestEdge( tree->root(), rwpos , dist,  pInfo);

	// set color as normalized distance to edge. Creates a gradient.
	if (pInfo.valid()) {
		double val = pInfo.distance() / maxDist;
		resultColor.x = val;
		resultColor.y = val;
		resultColor.z = val;
		resultAlpha = val;
	}

	///////////////////////////////////////////////////////////////
	MDataHandle outAlphaHandle = data.outputValue( aOutAlpha );
	float &outAlpha = outAlphaHandle.asFloat();
	outAlpha = resultAlpha;
	outAlphaHandle.setClean();

	MDataHandle outColorHandle = data.outputValue( aOutColor );
	MFloatVector &outColor = outColorHandle.asFloatVector();
	outColor = resultColor;
	outColorHandle.setClean();

	return MS::kSuccess;
}