/***************************************************************************
edgeTreeNode.h  -  description
-------------------
    begin                : Fri Mar 31 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com
	***************************************************************************/
#ifndef _edgeTreeNode
#define _edgeTreeNode

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include "edgeTreeData.h"
/// hello
class edgeTreeNode : public MPxNode
{
public:
	
	edgeTreeNode();
	///
	virtual				~edgeTreeNode();
	///
	virtual MStatus		compute( const MPlug& plug, MDataBlock& data );
	
	static  void*		creator();
	///
	static  MStatus		initialize();
	///
	static  MObject		aInput;
	///
	static  MObject		aOutput;
	
	static MTypeId	id;
	///
	
private:
		///
		edgeTreeData * m_pd;
	
};

#endif
