#ifndef edgeData_H
#define edgeData_H

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MDynSweptLine.h>
#include <maya/MBoundingBox.h>
#include <maya/MPointArray.h>
#include <maya/MVectorArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatPointArray.h>

#include <maya/MVector.h>
#include <maya/MDoubleArray.h>
#include <vector>

#include "errorMacros.h"
#include "mayaMath.h"
#include "pointOnEdgeInfo.h" 

using namespace std;

typedef mayaMath::axis axis;
 
class edgeData{

public:

	edgeData();

	edgeData(
	const MFloatPoint &startPoint,
	const MFloatPoint &endPoint,
	const MFloatArray &uvStart,
	const MFloatArray &uvEnd
	// int i
	);

	~edgeData();
	
	edgeData& operator=(const edgeData& other);

	MVector center() const;

	double center(axis a) const ;

	double min(axis a) const ;

	double max(axis a) const ;

	// int id() const;

	bool sphereIntersectsBB(const MFloatPoint &c,   float r) const  ;

	bool sphereIntersectsLine(const MFloatPoint &c, float r,  pointOnEdgeInfo & pInfo) const;
	
	const MBoundingBox & box() const ;

	const MFloatPoint & startPoint() const ;
	
	const MFloatPoint & endPoint() const ;

   	const MFloatArray & uvStart() const ;

   	const MFloatArray & uvEnd() const ;


private:
		
 	void computeBoundingBox();
   	
   	MFloatPoint m_startPoint;
   	MFloatPoint m_endPoint;
	MFloatArray m_uvStart;
	MFloatArray m_uvEnd;
	MBoundingBox m_box; 
	// int m_id;
};

#endif

